# Landscape Metrics Tools

This is a modification to the ZonalMetrics ArcGIS toolbox developed by J. Adamczyk and D. Tiede:
https://github.com/ZGIS/ZonalMetrics-Toolbox

This project was developed in the course of my master thesis with the support of my supervisor U. Walz (HTW Dresden) and M. Behnisch (IOER Dresden).

Following changes are deployed by now:
*  Migragion to Python 3 for usage in ArcGIS Pro
*  Added tools: meff, cbi, prox, wup
*  Added example data and ArcGIS Pro packages

Open as python toolbox via folder connection in ArcGIS

Detailed user manual is in file "UserManual_ZonalMetrics_Extension.pdf"

The directory "./Example_Geodatabases" contains example Geodatabases that can be used to test the tools.
