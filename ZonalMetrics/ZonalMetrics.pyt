﻿# coding=utf-8
import math
import operator
from collections import Counter, OrderedDict, defaultdict
import random
import sys
import os
from operator import itemgetter
import time
import matplotlib.pyplot as plt
import numpy
from scipy.spatial import distance
from wccValues import *

# from itertools import groupby
# from itertools import combinations
import arcpy

# noinspection PyUnresolvedReferences
from arcpy.da import SearchCursor, UpdateCursor, InsertCursor
import itertools
import imp

ZONE_AREA_FIELD_NAME = "zone_area"
UNIT_ID_FIELD_NAME = "unitID"

if 'tools' in sys.modules:
    imp.reload(sys.modules['tools'])
from tools import log, is_debug, get_field, intersect_analyzed_with_stat_layer, \
    FieldNotFoundException, ScriptParameters, \
    getParameterValues, handleException, delete_if_exists, createTempLayer, select_features_from_feature_class, \
    setup_debug_mode, enum, create_temp_layer_name, on_debug, get_scratchworkspace, log_debug

__authors_and_citation__ = 'Joanna Adamczyk, Dirk Tiede, ZonalMetrics - a Python toolbox for zonal landscape structure analysis, Computers & Geosciences, Volume 99, February 2017, Pages 91-99, ISSN 0098-3004, http://dx.doi.org/10.1016/j.cageo.2016.11.005'
__license___ = 'GPL-3 GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007'
__version__ = '1.0 for ArcGIS 10.1 or newer'

default_encoding = sys.getdefaultencoding()


# arcpy.env.scratchWorkspace = get_scratchworkspace()


class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the .pyt file)."""
        self.label = "ZonalMetrics Tools"
        self.alias = "zonalmetrics"

        # List of tool classes associated with this toolbox
        self.tools = [CreateHexagons,
                      CreatePie,
                      AreaMetrics,
                      DiversityMetricsTool,
                      EdgeMetricsTool,
                      MeanShapeIndex,
                      InterpersionJuxtaposition,
                      ConnectivityIndex,
                      EffectiveMeshSize,
                      ProximityIndex,
                      # UrbanSprawlMetrics,
                      UrbanSprawlMetricsRasterImpl,
                      LargestPatchIndex,
                      ContrastMetricsTool,
                      # MetricsGrouping,
                      ConnectanceMetricsTool,
                      ]


class LayerPrepare(object):
    """
    Prepares layer
    """

    def prepare(self, layer):
        raise NotImplementedError()

    def update_row(self, fields, row):
        pass


class EnsureFieldExists(LayerPrepare):
    def __init__(self, field_name, field_type, default_value=None, force_default_value=True):
        LayerPrepare.__init__(self)
        self.field_name = field_name
        self.field_type = field_type
        self.default_value = default_value
        self.field_added = False
        self.force_default_value = force_default_value

    def prepare(self, layer):
        try:
            return get_field(layer, self.field_name)
        except FieldNotFoundException:
            arcpy.AddField_management(layer,
                                      self.field_name,
                                      self.field_type,
                                      "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
            self.field_added = True
        return get_field(layer, self.field_name)

    def update_row(self, fields, row):
        LayerPrepare.update_row(self, fields, row)
        if (self.field_added or self.force_default_value) and self.default_value is not None:
            field_idx = fields[self.field_name]
            row[field_idx] = self.default_value


class EnsureZoneAreaFieldExists(EnsureFieldExists):
    def __init__(self):
        EnsureFieldExists.__init__(self, ZONE_AREA_FIELD_NAME, "DOUBLE", force_default_value=False)

    def update_row(self, fields, row):
        EnsureFieldExists.update_row(self, fields, row)
        if self.field_added:
            geometry_idx = fields['SHAPE@']
            zone_area_idx = fields[ZONE_AREA_FIELD_NAME]
            geometry = row[geometry_idx]
            row[zone_area_idx] = geometry.area


class EnsureUnitIDFieldExists(EnsureFieldExists):
    def __init__(self):
        EnsureFieldExists.__init__(self, UNIT_ID_FIELD_NAME, "LONG", force_default_value=False)

    def update_row(self, fields, row):
        EnsureFieldExists.update_row(self, fields, row)
        try:
            unit_id_idx = fields[UNIT_ID_FIELD_NAME]
        except FieldNotFoundException:
            unit_id_idx = 0
        if self.field_added or (row[unit_id_idx] == None):
            try:
                id_idx = fields['OBJECTID']
            except Exception as e:
                id_idx = fields['FID']
            row[unit_id_idx] = row[id_idx]


class MetricsCalcTool(object):
    def __init__(self):
        self.category = "ZonalMetrics"
        self.label = "FILL ME IN SUBCLASS"
        self.description = "FILL ME IN SUBCLASS"
        self.canRunInBackground = True
        self._temp_layers = []
        self._temp_layer_nb = 1

    def getParameterInfo(self):
        inputArea = arcpy.Parameter(
            displayName="Input layer",
            name="in_area",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Input")

        statLayer = arcpy.Parameter(
            displayName="Statistical layer",
            name="stat_layer",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Input")

        out = arcpy.Parameter(
            displayName="Output Feature",
            name="out_feature",
            datatype="DEFeatureClass",
            parameterType="Derived",
            direction="Output")

        out.parameterDependencies = [statLayer.name]
        out.schema.clone = True

        classField = arcpy.Parameter(
            displayName="Class Field",
            name="class_field",
            datatype="Field",
            parameterType="Required",
            category="Select Classes from Input Layer",
            direction="Input")
        classField.parameterDependencies = [inputArea.name]

        classList = arcpy.Parameter(
            displayName="Classes",
            name="class_list",
            datatype="GPString",
            parameterType="Optional",
            category="Select Classes from Input Layer",
            direction="Input",
            multiValue=True)
        classList.parameterDependencies = [classField.name]

        return [inputArea, statLayer, classField, classList, out]

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def get_all_classes(self, input_area, class_field_name, map_func=None):
        f = map_func
        if f is None:
            f = lambda x: x
        with SearchCursor(input_area, class_field_name) as cur:
            for row in cur:
                yield f(row[0])

    def _prepareClassListParmeterValues(self, parameter, inputFeature, fieldName, escape=None):
        parameter.filter.type = 'ValueList'
        uniqueValues = list(OrderedDict.fromkeys(self.get_all_classes(inputFeature, fieldName, map_func=str)).keys())
        if escape is not None:
            uniqueValues = [escape(value) for value in uniqueValues]
        if '' in uniqueValues:
            uniqueValues.remove('')
            parameter.setWarningMessage('Table contains empty class names which were removed from selection');
        parameter.filter.list = sorted(uniqueValues)

    def updateParameters(self, params):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        parameters = ScriptParameters(params)
        ''' Just update class_field/list if attribute is used'''
        if hasattr(parameters, 'class_field'):
            class_field_param = parameters.class_field
            class_list_param = parameters.class_list

            if class_field_param.altered:
                try:
                    fieldName = class_field_param.valueAsText
                    inputFeature = parameters.in_area.value
                    selected = getattr(class_list_param, 'values', None)
                    if selected:
                        all_classes = self.get_all_classes(inputFeature, fieldName, map_func=str)
                        class_list_param.values = list(
                            set(selected).intersection(set(all_classes)))
                    self._prepareClassListParmeterValues(class_list_param, inputFeature, fieldName, escape=None)
                except UnicodeEncodeError:
                    # will be handled in updateMessages
                    pass

        return

    def _escape(self, value):
        return value.replace('\\\\', '\\').replace("'", "\\'").replace(";", "\;")

    #
    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        parameters = ScriptParameters(parameters)

        # todo: change this to if(hasattr)
        try:
            input_feature = parameters.in_area.value
            zonal_param = parameters.in_area
            stat_param = parameters.stat_layer

            if zonal_param.value is None:
                zonal_param.clearMessage()
            else:
                desc = arcpy.Describe(zonal_param.value)
                feature_type = desc.shapeType.lower()
                file_extension = desc.extension
                if not feature_type == "polygon":
                    zonal_param.setErrorMessage("Only polygon-features allowed")

            if stat_param.value is None:
                stat_param.clearMessage()
            else:
                desc = arcpy.Describe(stat_param.value)
                feature_type = desc.shapeType.lower()
                file_extension = desc.extension
                if not feature_type == "polygon":
                    stat_param.setErrorMessage("Only polygon-features allowed")
        except AttributeError:
            pass

        if hasattr(parameters, 'class_field'):
            class_field_param = parameters.class_field
            field_name = class_field_param.valueAsText
            if field_name:
                self.get_all_classes(input_feature, field_name, map_func=str)
                with SearchCursor(input_feature, field_name) as cur:
                    i = 0
                    for row in cur:
                        val = row[0]
                        if not isinstance(val, str):
                            val = str(val)
                        if len(val.strip()) == 0:
                            msg = 'Column ' + field_name + ' contains empty/blank values in row ' + str(i)
                            class_field_param.setWarningMessage(msg)
                        try:
                            val.encode(default_encoding, 'strict')
                        except UnicodeEncodeError:
                            msg = 'Column ' + field_name + ' contains illegal character in name in row ' + str(
                                i) + '(' + val.encode(default_encoding, 'replace') + ')'
                            class_field_param.setErrorMessage(msg)
                        i += 1

    @staticmethod
    def prepare_stat_layer(layer, *prepares):
        for prepare in prepares:
            assert isinstance(prepare, LayerPrepare)
            prepare.prepare(layer)
        with UpdateCursor(layer, ['*', 'SHAPE@']) as cur:
            fields = OrderedDict(list(zip(cur.fields, list(range(0, len(cur.fields))))))
            for row in cur:
                for prepare in prepares:
                    prepare.update_row(fields, row)
                cur.updateRow(row)

    def create_temp_layer(self, prefix):
        layer = create_temp_layer_name(prefix=prefix)
        self._temp_layer_nb += 1
        self._temp_layers.append(layer)
        return layer

    def on_exit(self):
        on_debug(*self._temp_layers)
        delete_if_exists(*self._temp_layers)


class MetricsGrouping(MetricsCalcTool):
    def __init__(self):
        MetricsCalcTool.__init__(self)
        self.category = "Metrics"
        self.label = "Calculate multiple metrics"

        subcls = MetricsCalcTool.__subclasses__()
        subcls.remove(ContrastMetricsTool)
        subcls.remove(MetricsGrouping)
        subcls.remove(ConnectanceMetricsTool)

        self.tools = [t() for t in subcls]

        self.description = "Calculates selected metrics:<ul>"
        for t in self.tools:
            self.description += "<li>" + t.label + "</li>"
        self.description += "</ul>"

        self.canRunInBackground = True

    def getParameterInfo(self):
        params = MetricsCalcTool.getParameterInfo(self)
        toolsSelection = arcpy.Parameter(
            displayName="Tools to be run",
            name="selected_tools",
            datatype="GPString",
            parameterType="Required",
            direction="Input",
            multiValue=True)
        toolsSelection.filter.type = 'ValueList'
        toolsSelection.filter.list = sorted([tool.label for tool in self.tools])
        return [toolsSelection] + params

    def execute(self, parameters, messages):
        setup_debug_mode()
        params = ScriptParameters(parameters)
        selected_tools = params.selected_tools.values
        for tool in self.tools:
            if tool.label not in selected_tools:
                continue
            tool_parameters = parameters[1:]
            log('\n\t---')
            log('\tRunning tool %s' % tool.label)
            log('\t---')
            tool.execute(tool_parameters, messages)
        return

        # subclasses


AreaCalcMethod = enum(CUT_TO_STAT=0, OVERLAP=1, CENTROID=2)
AreaCalcMethod_descriptions = {AreaCalcMethod.CUT_TO_STAT: 'Cut patches',
                               AreaCalcMethod.OVERLAP: 'Select overlapping patches',
                               AreaCalcMethod.CENTROID: 'Select by centroids'}


class AbstractAreaMetricsCalcTool(MetricsCalcTool):
    input_layer = None
    statistics_layer = None
    class_field = None
    selected_classes = None
    input_parameters = None
    area_analysis_method = AreaCalcMethod.CUT_TO_STAT
    process_only_selected = False
    to_cleanup = []
    allow_percent_gt_100 = False

    def __init__(self):
        super(AbstractAreaMetricsCalcTool, self).__init__()

    def getParameterInfo(self):
        params = MetricsCalcTool.getParameterInfo(self)
        area_analysis_method_param = arcpy.Parameter(
            displayName="Area analysis method",
            name="area_analysis_method",
            datatype="GPString",
            parameterType="Required",
            direction="Input",
            multiValue=False)
        area_analysis_method_param.filter.type = 'ValueList'
        area_analysis_method_param.filter.list = list(AreaCalcMethod_descriptions.values())
        area_analysis_method_param.value = area_analysis_method_param.filter.list[0]
        return params + [area_analysis_method_param]

    def _parse_parameters(self, parameters):
        self.input_parameters = ScriptParameters(parameters)
        self.input_layer = self.input_parameters.in_area.valueAsText
        self.statistics_layer = self.input_parameters.stat_layer.valueAsText
        class_field_name = self.input_parameters.class_field.valueAsText
        if class_field_name:
            self.class_field = get_field(self.input_layer, class_field_name)
            self.selected_classes = getParameterValues(self.input_parameters.class_list)
        area_analysis_method_value = self.input_parameters.area_analysis_method.valueAsText
        self.area_analysis_method = self.input_parameters.area_analysis_method.filter.list.index(
            area_analysis_method_value)

    def execute(self, parameters, messages):
        setup_debug_mode()
        self._parse_parameters(parameters)

        try:
            self.prepare_stat_layer(self.statistics_layer,
                                    EnsureUnitIDFieldExists(), )
            self.prepare()
            self.process_layer_to_analyse()
        except Exception as e:
            handleException(e)
            raise e
        finally:
            self.cleanup()

    def prepare(self):
        pass

    def update_statistic_row(self, statistic_layer_row, stat_fields, patches_cursor):
        raise NotImplemented()

    def dump_selected_hexagons(self, layer_to_analyse_hids):
        if is_debug():
            selected_hexes = create_temp_layer_name('a_03_selected_hexes')
            self.to_cleanup.append(selected_hexes)
            selected_hexes_out = create_temp_layer_name('a_04_selected_hexes')
            self.to_cleanup.append(selected_hexes_out)

            hids = []
            with SearchCursor(layer_to_analyse_hids, ['stat_id']) as c:
                for row in c:
                    hids.append(str(row[0]))
            arcpy.MakeFeatureLayer_management(self.statistics_layer, selected_hexes)

            where_hids = " \"unitID\" in (%s)" % ", ".join(hids)
            arcpy.SelectLayerByAttribute_management(selected_hexes, "ADD_TO_SELECTION", where_hids)

            arcpy.CopyFeatures_management(selected_hexes, selected_hexes_out)

            on_debug(selected_hexes, selected_hexes_out)

    def _get_input_filtered_to_selection(self):
        log('Selecting features from input layer')
        input_selected = create_temp_layer_name('a_01_1_input_selected')
        self.to_cleanup.append(input_selected)
        if self.class_field is not None and self.selected_classes and len(self.selected_classes) > 0:
            select_features_from_feature_class(self.class_field, self.selected_classes, self.input_layer,
                                               input_selected)
        else:
            arcpy.CopyFeatures_management(self.input_layer, input_selected)
        on_debug(input_selected)
        return input_selected

    def process_layer_to_analyse(self):
        log('Creating intersection of statistics with input layer')

        input_selected = self._get_input_filtered_to_selection()

        layer_to_analyse = create_temp_layer_name('a_02_layer_to_analyse')
        self.to_cleanup.append(layer_to_analyse)

        layer_to_analyse_sorted = create_temp_layer_name('a_03_layer_to_analyse_sorted')
        self.to_cleanup.append(layer_to_analyse_sorted)

        # name of field with FID of found patch in patch_search_layer
        patch_search_field = None
        # name of field with FID of found patch in stat_search_layer
        patch_stat_search_field = None
        # name of field with id of statistic patch (i.e hexagon)
        stat_field = None
        # layer where to search for found hexagons and matched patches
        stat_search_layer = layer_to_analyse_sorted
        # layer where to search for patches to analyse
        patch_search_layer = None

        log('Area analysis method: {}, {}'.format(self.area_analysis_method,
                                                  AreaCalcMethod_descriptions[self.area_analysis_method]))

        if self.area_analysis_method == AreaCalcMethod.CUT_TO_STAT:
            arcpy.Intersect_analysis([self.statistics_layer, input_selected], layer_to_analyse)

            ##
            arcpy.AddField_management(layer_to_analyse, 'sortFID', "LONG")
            desc = arcpy.Describe(layer_to_analyse)
            IDfield = desc.OIDFieldName
            arcpy.CalculateField_management(layer_to_analyse, 'sortFID',
                                            "!" + IDfield + "!",
                                            "PYTHON_9.3")
            patch_stat_search_field = 'sortFID'

            ##
            # patch_stat_search_field = 'FID'#

            # patch_search_field = 'OBJECTID'
            patch_search_field = arcpy.Describe(layer_to_analyse).OIDFieldName
            stat_field = UNIT_ID_FIELD_NAME
            stat_search_layer = layer_to_analyse_sorted
            patch_search_layer = layer_to_analyse
            self.allow_percent_gt_100 = False

        elif self.area_analysis_method == AreaCalcMethod.OVERLAP:
            match_option = 'INTERSECT'
            log('Joining {} with {} using {}'.format(self.statistics_layer, input_selected, match_option))
            arcpy.SpatialJoin_analysis(self.statistics_layer, input_selected, layer_to_analyse,
                                       join_operation='JOIN_ONE_TO_MANY', join_type='KEEP_COMMON',
                                       match_option=match_option)

            patch_stat_search_field = 'JOIN_FID'
            """
            try:
                patch_search_field = 'FID'
            except Exception:
                patch_search_field = 'OID'
            """
            patch_search_field = arcpy.Describe(layer_to_analyse).OIDFieldName
            stat_field = UNIT_ID_FIELD_NAME
            stat_search_layer = layer_to_analyse_sorted
            patch_search_layer = input_selected
            self.allow_percent_gt_100 = True

        elif self.area_analysis_method == AreaCalcMethod.CENTROID:
            exploded_polygons = create_temp_layer_name('a_05_singleparts')
            centroids = create_temp_layer_name('a_06_centroids')
            self.to_cleanup.append(exploded_polygons)
            self.to_cleanup.append(centroids)

            arcpy.MultipartToSinglepart_management(input_selected, exploded_polygons)
            arcpy.FeatureToPoint_management(exploded_polygons, centroids)
            on_debug(exploded_polygons, centroids)

            match_option = 'INTERSECT'
            log('Joining {} with {} using {}'.format(self.statistics_layer, centroids, match_option))
            arcpy.SpatialJoin_analysis(self.statistics_layer, centroids, layer_to_analyse,
                                       join_operation='JOIN_ONE_TO_MANY', join_type='KEEP_COMMON',
                                       match_option=match_option)
            patch_stat_search_field = 'ORIG_FID'
            '''
            try:
                patch_search_field = 'FID'
            except Exception:
                patch_search_field = 'OBJECTID'
            '''

            patch_search_field = arcpy.Describe(layer_to_analyse).OIDFieldName
            stat_field = UNIT_ID_FIELD_NAME
            stat_search_layer = layer_to_analyse_sorted
            patch_search_layer = input_selected
            self.allow_percent_gt_100 = True

        log('Sorting {} by fields {}, {}'.format(layer_to_analyse, stat_field, patch_stat_search_field))
        arcpy.Sort_management(layer_to_analyse, layer_to_analyse_sorted,
                              [[stat_field, 'ASCENDING'], [patch_stat_search_field, 'ASCENDING']])

        on_debug(layer_to_analyse, layer_to_analyse_sorted)
        log(
            """patch_stat_search_field: {},
            stat_field: {},
            stat_search_layer: {},
            patch_search_layer: {},
            patch_search_field: {}""".format(
                patch_stat_search_field,
                stat_field,
                stat_search_layer,
                patch_search_layer,
                patch_search_field))

        def update_stats(stats_where_clause=None):
            with UpdateCursor(self.statistics_layer, [stat_field, '*', 'SHAPE@AREA', 'SHAPE@', 'SHAPE@LENGTH'],
                              where_clause=stats_where_clause) as stat_update_cursor:
                stat_fields = stat_update_cursor.fields
                for statistic_layer_row in stat_update_cursor:
                    arcpy.SetProgressorPosition()
                    stat_id = statistic_layer_row[0]
                    patch_w_clause = '{field} = {value}'.format(
                        field=arcpy.AddFieldDelimiters(stat_search_layer, stat_field),
                        value=stat_id)
                    arcpy.AddMessage(patch_w_clause)
                    patches_ids = [str(row[1]) for row in
                                   SearchCursor(stat_search_layer, [stat_field, patch_stat_search_field],
                                                where_clause=patch_w_clause)]
                    returned_row = None
                    if patches_ids:
                        log('stat_id: {sid},\n    patches ids: {pids}'.format(sid=stat_id, pids=patches_ids))
                        patch_w_clause = '{field} IN ({values})'.format(
                            field=arcpy.AddFieldDelimiters(patch_search_layer, patch_search_field),
                            values=','.join(patches_ids))
                        with SearchCursor(patch_search_layer,
                                          [patch_search_field, '*', 'SHAPE@AREA', 'SHAPE@', 'SHAPE@LENGTH'],
                                          where_clause=patch_w_clause) as patches_cursor:
                            returned_row = self.update_statistic_row(statistic_layer_row, stat_fields, patches_cursor)
                    else:
                        returned_row = self.update_statistic_row(statistic_layer_row, stat_fields, [])

                    if returned_row:
                        stat_update_cursor.updateRow(returned_row)

        msg = 'Start of area calculation'
        log(msg)
        if self.process_only_selected:
            arcpy.SetProgressor('default ', msg, 0, 100, 1)
            for stat_ids in _get_distinct_field_values(stat_search_layer, stat_field):
                ids = ', '.join([str(sid) for sid in stat_ids])
                stats_w_clause = '{field} IN ({values})'.format(
                    field=arcpy.AddFieldDelimiters(stat_search_layer, stat_field),
                    values=ids)
                update_stats(stats_w_clause)
        else:
            arcpy.SetProgressor('step', msg, 0, int(arcpy.GetCount_management(self.statistics_layer).getOutput(0)), 1)
            update_stats()
        log('Finished area calculation')
        arcpy.ResetProgressor()
        return None

    def cleanup(self):
        delete_if_exists(self.to_cleanup)

    def prepare_stat_field(self, name, field_type):
        def add_field():
            arcpy.AddField_management(self.statistics_layer, name, field_type, '', '', '', '', "NULLABLE",
                                      "NON_REQUIRED", '')
            return get_field(self.statistics_layer, name)

        return get_field(self.statistics_layer, name, on_not_exists=add_field)

    def _get_distinct_field_values(stat_search_layer, stat_field):
        max_records = 50
        recs = set()
        with SearchCursor(stat_search_layer, [stat_field]) as sc:
            for row in sc:
                recs.add(row[0])
                if len(recs) == max_records:
                    yield recs
                    recs = set()
            if len(recs) > 0:
                yield recs


class AreaMetrics(AbstractAreaMetricsCalcTool):
    fields = {}
    _classes_template = {}

    def __init__(self):
        AbstractAreaMetricsCalcTool.__init__(self)
        self.label = "Area metrics"
        self.description = '''Calculates area metrics'''

    def prepare(self):
        classes = self.selected_classes
        if not self.selected_classes:
            classes = set(self.get_all_classes(self.input_layer, self.class_field.name))

        for cl_raw in classes:
            cl = str(cl_raw).strip("'")
            area_field_name = arcpy.ValidateFieldName('ca%s' % (cl,))[:10]
            count_field_name = arcpy.ValidateFieldName('npc%s' % (cl,))[:10]
            percent_field_name = arcpy.ValidateFieldName('pz%s' % (cl,))[:10]
            self.fields[cl] = {'area': area_field_name, 'count': count_field_name, 'percent': percent_field_name}
            self._classes_template[cl] = 0
        for _, field_names in list(self.fields.items()):
            for field_name in list(field_names.values()):
                self.prepare_stat_field(field_name, 'DOUBLE')
        self.prepare_stat_field(ZONE_AREA_FIELD_NAME, 'DOUBLE')

    def update_statistic_row(self, statistic_layer_row, statistic_layer_fields, patches_cursor):
        stat_area_field_idx = statistic_layer_fields.index('SHAPE@AREA')
        zone_area_field_idx = statistic_layer_fields.index(ZONE_AREA_FIELD_NAME)

        patches_area = Counter(self._classes_template)
        patches_count = Counter(self._classes_template)

        if patches_cursor:
            class_field_idx = patches_cursor.fields.index(self.class_field.name)
            area_field_idx = patches_cursor.fields.index('SHAPE@AREA')
            for patch in patches_cursor:
                class_name = str(patch[class_field_idx])
                patches_area[class_name] += patch[area_field_idx]
                patches_count[class_name] += 1

        for cl_raw in list(patches_count.keys()):
            cl = str(cl_raw)
            this_class_field_names = self.fields[cl]
            area_field_name = this_class_field_names['area']
            count_field_name = this_class_field_names['count']
            percent_field_name = this_class_field_names['percent']
            patches_area_in_hex = patches_area[cl]
            stat_area = statistic_layer_row[stat_area_field_idx]
            statistic_layer_row[statistic_layer_fields.index(area_field_name)] = patches_area_in_hex
            statistic_layer_row[statistic_layer_fields.index(count_field_name)] = patches_count[cl]
            percent = round((patches_area_in_hex / stat_area) * 100, 3)
            if not self.allow_percent_gt_100 and percent > 100:
                log('Warning: percent of area bigger than 100 for row: %s' % str(statistic_layer_row))
            statistic_layer_row[statistic_layer_fields.index(percent_field_name)] = percent

        statistic_layer_row[zone_area_field_idx] = statistic_layer_row[stat_area_field_idx]
        return statistic_layer_row


class LargestPatchIndex(AbstractAreaMetricsCalcTool):
    lpi_field = None
    lpi_class_field = None

    def __init__(self):
        AbstractAreaMetricsCalcTool.__init__(self)
        self.label = "Area Metrics - Largest Patch Index"
        self.description = '''Looks for patch covering the largest area within the statistical zone,
        calculates the area of this patch (LPI) and identifies the class this patch belongs to.
        If proportion of land cover within the statistical zone is considered,
        all patches of considered classes should be dissolved.'''
        self.process_only_selected = False

    def getParameterInfo(self):
        params = AbstractAreaMetricsCalcTool.getParameterInfo(self)
        merge_same_class_patches_param = arcpy.Parameter(
            displayName="Merge patches of the same class",
            name="merge_same_class_patches",
            datatype="GPBoolean",
            parameterType="Required",
            direction="Input",
            multiValue=False)
        merge_same_class_patches_param.value = False
        params.insert(-2, merge_same_class_patches_param)
        return params

    def _parse_parameters(self, parameters):
        AbstractAreaMetricsCalcTool._parse_parameters(self, parameters)
        self.merge_same_class_patches = self.input_parameters.merge_same_class_patches.value
        log('merge_same_class_patches=%s' % self.merge_same_class_patches)

    def _get_input_filtered_to_selection(self):
        input_filtered = AbstractAreaMetricsCalcTool._get_input_filtered_to_selection(self)

        input_merged = create_temp_layer_name('a_01_2_input_merged')
        self.to_cleanup.append(input_merged)

        if self.merge_same_class_patches:
            arcpy.Dissolve_management(in_features=input_filtered,
                                      out_feature_class=input_merged,
                                      dissolve_field=self.input_parameters.class_field.valueAsText,
                                      multi_part=False
                                      )
        else:
            arcpy.CopyFeatures_management(input_filtered, input_merged)
        on_debug(input_merged)

        return input_merged

    def prepare(self):
        self.lpi_field = self.prepare_stat_field('lpi', 'DOUBLE')
        self.lpi_class_field = self.prepare_stat_field('lpi_class', 'STRING')

    def update_statistic_row(self, statistic_layer_row, statistic_layer_fields, patches_cursor):
        stat_area_field_idx = statistic_layer_fields.index('SHAPE@AREA')
        lpi_field_idx = statistic_layer_fields.index(self.lpi_field.name)
        lpi_class_field_idx = statistic_layer_fields.index(self.lpi_class_field.name)

        statistic_layer_row[lpi_field_idx] = 0
        statistic_layer_row[lpi_class_field_idx] = ''

        if not patches_cursor:
            return statistic_layer_row

        area_field_idx = patches_cursor.fields.index('SHAPE@AREA')
        class_field_idx = patches_cursor.fields.index(self.class_field.name)

        largest_patch = None

        for patch in patches_cursor:
            patch_area = patch[area_field_idx]
            if largest_patch is None or patch_area > largest_patch[area_field_idx]:
                largest_patch = patch
        if largest_patch is None:
            return statistic_layer_row

        statistic_layer_row[lpi_field_idx] = round(
            largest_patch[area_field_idx] / statistic_layer_row[stat_area_field_idx] * 100, 3)
        statistic_layer_row[lpi_class_field_idx] = largest_patch[class_field_idx]

        return statistic_layer_row


class DiversityMetricsTool(MetricsCalcTool):
    diversity_field_name = 'shdi'

    def __init__(self):
        MetricsCalcTool.__init__(self)
        self.label = "Diversity Metrics"
        self.description = '''Calculates Shannon diversity index (SHDI) per zone'''

    @staticmethod
    def _get_class_area_for_zone(input_stat_layer_intersect, unit_id, class_field_name):
        class_area_counter = Counter()
        where = "%s=%s" % (UNIT_ID_FIELD_NAME, unit_id)
        with SearchCursor(input_stat_layer_intersect, (class_field_name, "SHAPE@AREA"), where) as cur2:
            for className, area in cur2:
                class_area_counter[className] += area
        return class_area_counter

    @staticmethod
    def _calculate_shdi(class_areas):
        shdi_raw = 0

        classes_areas = [area for area in list(class_areas.values()) if area > 0]
        class_area_per_zone = sum(classes_areas)

        for class_area in classes_areas:
            shdi_raw += class_area / class_area_per_zone * math.log(class_area / class_area_per_zone)
        shdi = -shdi_raw
        if is_debug():
            log_debug('shdi=%f, zone_area=%d, classes_areas=%s' % (shdi, class_area_per_zone, classes_areas))
        return shdi

    def execute(self, parameters, messages):
        setup_debug_mode()
        input_parameters = ScriptParameters(parameters)
        input_area = input_parameters.in_area.valueAsText
        stat_layer = input_parameters.stat_layer.valueAsText
        class_field_name = input_parameters.class_field.valueAsText
        class_field = get_field(input_area, class_field_name)
        class_list = getParameterValues(input_parameters.class_list)
        self.prepare_stat_layer(stat_layer,
                                EnsureUnitIDFieldExists(),
                                EnsureFieldExists(
                                    field_name=self.diversity_field_name,
                                    field_type='DOUBLE',
                                    default_value=0
                                ),
                                EnsureZoneAreaFieldExists(),

                                )

        msg = "Counting diversity of classes in hexagons"
        log(msg)
        arcpy.SetProgressor("step", msg, 0, int(arcpy.GetCount_management(stat_layer).getOutput(0)), 1)

        fields = [UNIT_ID_FIELD_NAME, "SHAPE@", ZONE_AREA_FIELD_NAME, self.diversity_field_name]
        with intersect_analyzed_with_stat_layer(stat_layer, input_area,
                                                class_field=class_field,
                                                class_list=class_list) as input_stat_layer_intersect, \
                UpdateCursor(stat_layer, fields) as cur:
            for row in cur:
                arcpy.SetProgressorPosition()
                try:
                    zone_area = row[2]
                    if zone_area == 0:
                        continue
                    class_area_counter = self._get_class_area_for_zone(input_stat_layer_intersect, row[0],
                                                                       class_field_name)
                    row[3] = self._calculate_shdi(class_area_counter)
                    cur.updateRow(row)
                except Exception as e:
                    arcpy.AddError('Error: ' + str(e))
        arcpy.ResetProgressor()


class EdgeMetricsTool(MetricsCalcTool):
    density_area = arcpy.Parameter(
        displayName="Edge Density calculation area (ha)",
        name="density_area",
        datatype="GPLong",
        parameterType="Required",
        direction="Input")
    density_area.value = 1000

    edge_length_field = 'tc_edge'
    edge_density_field = 'ed'

    def __init__(self):
        MetricsCalcTool.__init__(self)
        self.label = 'Edge Metrics'
        self.description = 'Calculates Class Edge lenght (TcE) for all patches of selected classes within the statistical zones'

    def getParameterInfo(self):
        params = MetricsCalcTool.getParameterInfo(self)
        params.append(self.density_area)
        return params

    def execute(self, parameters, messages):
        setup_debug_mode()
        input_parameters = ScriptParameters(parameters)
        input_area = input_parameters.in_area.valueAsText
        stat_layer = input_parameters.stat_layer.valueAsText
        classFieldName = input_parameters.class_field.valueAsText
        classField = get_field(input_area, classFieldName)
        classList = getParameterValues(input_parameters.class_list)

        density_area_ha = int(input_parameters.density_area.valueAsText)  # in ha
        density_area = density_area_ha * 10000  # 1 ha = 10000 square meters

        self.prepare_stat_layer(stat_layer,
                                EnsureUnitIDFieldExists(),
                                EnsureZoneAreaFieldExists(),
                                EnsureFieldExists(self.edge_length_field, "DOUBLE", 0),
                                EnsureFieldExists(self.edge_density_field, "DOUBLE", 0),
                                )

        msg = "Counting edges lengths"
        log(msg)
        arcpy.SetProgressor("step", msg, 0, int(arcpy.GetCount_management(stat_layer).getOutput(0)), 1)

        edgesLayerName = "in_memory\\inputEdges%d" % random.randint(0, 1000000)
        edgesLayerNameLyr = edgesLayerName + 'Layer'
        try:
            arcpy.FeatureToLine_management(input_area, edgesLayerName)
            arcpy.MakeFeatureLayer_management(edgesLayerName, edgesLayerNameLyr)

            stat_layer_fields = [UNIT_ID_FIELD_NAME,
                                 self.edge_length_field,
                                 ZONE_AREA_FIELD_NAME,
                                 "SHAPE@AREA",
                                 self.edge_density_field]
            with intersect_analyzed_with_stat_layer(stat_layer, edgesLayerNameLyr, class_field=classField,
                                                    class_list=classList) as edgesIntersection, \
                    UpdateCursor(stat_layer, stat_layer_fields) as cur:
                for row in cur:
                    arcpy.SetProgressorPosition()
                    try:
                        where = "%s=%s" % (UNIT_ID_FIELD_NAME, row[0])
                        with SearchCursor(edgesIntersection, ("SHAPE@LENGTH",), where) as cur2:
                            edges_length = sum(edges_row[0] for edges_row in cur2)
                            row[1] = edges_length
                            zone_area = row[2]
                            row[4] = edges_length * density_area / zone_area
                        cur.updateRow(row)
                    except Exception as e:
                        arcpy.AddError('Error: ' + str(e))
        finally:
            # cleanup
            arcpy.Delete_management(edgesLayerName)
            arcpy.Delete_management(edgesLayerNameLyr)
        arcpy.ResetProgressor()


class MeanShapeIndex(AbstractAreaMetricsCalcTool):
    fields = {}
    _classes_template = {}
    shape_index_field_name = None

    def __init__(self):
        AbstractAreaMetricsCalcTool.__init__(self)
        self.label = "Mean Shape Index"
        self.description = '''Calculates Mean Shape Index for each statistical zone'''

    def prepare(self):
        classes = self.selected_classes
        if not self.selected_classes:
            classes = set(self.get_all_classes(self.input_layer, self.class_field.name))

        self.shape_index_field_name = self.prepare_stat_field('msi', 'DOUBLE')

        for _, field_names in list(self.fields.items()):
            for field_name in list(field_names.values()):
                self.prepare_stat_field(field_name, 'DOUBLE')

        self.prepare_stat_field(ZONE_AREA_FIELD_NAME, 'DOUBLE')

    def update_statistic_row(self, statistic_layer_row, statistic_layer_fields, patches_cursor):
        stat_area_field_idx = statistic_layer_fields.index('SHAPE@AREA')
        zone_area_field_idx = statistic_layer_fields.index(ZONE_AREA_FIELD_NAME)
        msi_idx = statistic_layer_fields.index(self.shape_index_field_name.name)
        patches_shape_index = Counter(self._classes_template)
        patches_count = Counter(self._classes_template)

        if patches_cursor:
            class_field_idx = patches_cursor.fields.index(self.class_field.name)
            geom_field_idx = patches_cursor.fields.index('SHAPE@')

            for patch in patches_cursor:
                class_name = str(patch[class_field_idx])
                current_patch = patch[geom_field_idx]
                current_patch_area = current_patch.getArea('GEODESIC', 'SQUAREKILOMETERS')
                current_patch_length = current_patch.getLength('GEODESIC', 'KILOMETERS')
                ##
                try:
                    shape_index_current_patch = (current_patch_length) / (
                            2 * (math.sqrt(math.pi * current_patch_area)))  # calculating SHAPE-INDEX!
                except ZeroDivisionError:
                    shape_index_current_patch = 0
                ##
                patches_shape_index[
                    class_name] += shape_index_current_patch  # adds shape_index of current patch to list of respective class
                patches_count[class_name] += 1

        count_patches_total = 0
        sum_si_total = 0

        for cl_raw in list(patches_count.keys()):
            cl = str(cl_raw)
            sum_si_total += patches_shape_index[cl]  # add sum of patch-shape-indicies of current class to a list
            count_patches_total += patches_count[cl]  # add number of patches-shape of current class to a list
        try:
            mean_shape_index = sum_si_total / count_patches_total  # calculating mean shape index of a unit
        except ZeroDivisionError:
            mean_shape_index = 0

        statistic_layer_row[msi_idx] = mean_shape_index
        return statistic_layer_row


class ConnectivityIndex(MetricsCalcTool):
    fields = {}
    _classes_template = {}
    cbi_field = 'CBIpatch'
    interPatch_field = 'INTERPATCH'
    intraPatch_field = 'INTRAPATCH'
    totalArea_field = 'areaTotal'
    intraStat_field = 'INTRAstat'
    interStat_field = 'INTERstat'
    cbi_stat_field = 'CBIstat'
    zoneArea_field = 'Astat'
    BUFFER_COUNT_FIELD = 'COUNT_PID'
    buffer_id_field = 'PID'
    # minMaxCBD_field = 'minMax_CBD' #calculating percantage-index out of buffered and unbuffered input polygons
    # cbdStatUnit_field = 'stat_CBD'

    road_layer = arcpy.Parameter(
        displayName="Line Barrier Layer (like roads)",
        name="road_layer",
        datatype="GPFeatureLayer",
        parameterType="Required",
        category="Barriers",
        direction="Input")

    settlement_layer = arcpy.Parameter(
        displayName="Polygon Barrier Layer (like settlements)",
        name="settlement_layer",
        datatype="GPFeatureLayer",
        parameterType="Optional",
        category="Barriers",
        direction="Input")

    line_buffer_size = arcpy.Parameter(
        displayName="Line Buffer Size",
        name="line_buffer_size",
        datatype="GPDouble",
        parameterType="Required",
        category="Barriers",
        direction="Input")
    line_buffer_size.value = 0.1

    buffer_size = arcpy.Parameter(
        displayName="Buffer Size",
        name="buffer_size",
        datatype="GPLong",
        parameterType="Required",
        direction="Input")
    buffer_size.value = 50

    def __init__(self):
        MetricsCalcTool.__init__(self)
        self.label = "Connectivity Index"
        self.description = '''Calculates Connectivity Index for each statistical zone'''

    def getParameterInfo(self):
        params = MetricsCalcTool.getParameterInfo(self)
        params.append(self.road_layer)
        params.append(self.line_buffer_size)
        params.append(self.settlement_layer)
        params.append(self.buffer_size)
        return params

    def generate_barrierLayer_CBI(self, road_layer, line_buffer_size, settlement_layer, stat_layer):
        log("Preparing barrier layer")
        roads_buffered = self.create_temp_layer('roads_buffered')
        arcpy.Buffer_analysis(road_layer, roads_buffered, line_buffer_size, "FULL", "ROUND", "ALL")
        barrier_layer = self.create_temp_layer('barrier_layer')
        if settlement_layer is not None:
            arcpy.Merge_management([settlement_layer, roads_buffered], barrier_layer)
        else:
            barrier_layer = roads_buffered
        return barrier_layer, roads_buffered

    def prepare_InputLayer_CBI(self, input_area, stat_layer, class_field, class_list, road_layer=None):
        log("Preparing input layer")
        OID_Field = arcpy.Describe(input_area).OIDFieldName
        input_layer_selected = self.create_temp_layer('inputLayer_selected')
        if class_field is not None and class_list and len(class_list) > 0:
            select_features_from_feature_class(class_field, class_list, input_area,
                                               input_layer_selected)
        else:
            arcpy.CopyFeatures_management(input_area, input_layer_selected)
        input_stat_intersect = self.create_temp_layer('inputStatintersectLyr')
        arcpy.Intersect_analysis([input_layer_selected, stat_layer], input_stat_intersect)
        input_layer_erased = self.create_temp_layer('inputLayer_erased')
        arcpy.Erase_analysis(input_stat_intersect, road_layer, input_layer_erased)
        single_part_input_features = self.create_temp_layer('single_part_input_features')
        arcpy.MultipartToSinglepart_management(input_layer_erased, single_part_input_features)
        arcpy.AddField_management(single_part_input_features, "ID_element", "LONG")
        with arcpy.da.UpdateCursor(single_part_input_features, ["ID_element", OID_Field]) as cursor:
            for row in cursor:
                row[0] = row[1]
                cursor.updateRow(row)
        del cursor
        return single_part_input_features

    def generateBuffer_CBI(self, input_layer_processed, stat_layer, barrier_layer, buffer_size):
        log("Generating Connectivities")
        OID_Field = arcpy.Describe(input_layer_processed).OIDFieldName
        # buffer input features
        buffer_layer = self.create_temp_layer('buffer_layer')
        arcpy.Buffer_analysis(input_layer_processed, buffer_layer, buffer_size, "FULL", "ROUND", "NONE")
        '''Repair broken geometry somehow helps with 999999 bug of ArcGIS Pro'''
        arcpy.RepairGeometry_management(buffer_layer)

        buffers_clipped_zone = self.create_temp_layer('buffers_clipped_zone')
        arcpy.Clip_analysis(buffer_layer, stat_layer, buffers_clipped_zone)
        # erase barriers
        buffer_erased_layer = self.create_temp_layer('buffers_erased_layer')
        arcpy.Erase_analysis(buffers_clipped_zone, barrier_layer, buffer_erased_layer)
        # arcpy.Erase_analysis(buffer_layer, barrier_layer, buffer_erased_layer)
        erased_singlepart_layer = self.create_temp_layer("erased_single")
        arcpy.MultipartToSinglepart_management(buffer_erased_layer, erased_singlepart_layer)
        # spatial join to eliminate all fake connections:
        buffer_notBuffer_join = self.create_temp_layer('buf_nBuf_join')
        arcpy.SpatialJoin_analysis(erased_singlepart_layer, input_layer_processed, buffer_notBuffer_join,
                                   "JOIN_ONE_TO_MANY", "KEEP_COMMON", "", "INTERSECT")
        buffer_notBuffer_joinFC = self.create_temp_layer('buffer_nBuf_join_FC')
        arcpy.MakeFeatureLayer_management(buffer_notBuffer_join, buffer_notBuffer_joinFC)
        arcpy.SelectLayerByAttribute_management(buffer_notBuffer_joinFC, "NEW_SELECTION", ' "ID_element" = "JOIN_FID" ')
        buffer_notBuffer_join_selected = self.create_temp_layer('buf_nBuf_join_selected')
        arcpy.CopyFeatures_management(buffer_notBuffer_joinFC, buffer_notBuffer_join_selected)
        buffer_layer_dissolved = self.create_temp_layer('buffer_layer_dissolved')
        # dissolving of the single part buffers :
        arcpy.Dissolve_management(buffer_notBuffer_join_selected, buffer_layer_dissolved, "", "", "SINGLE_PART")
        # add further primary key field to buffer layer because FID/OID not consistent:
        arcpy.AddField_management(buffer_layer_dissolved, "PID", "LONG")
        with arcpy.da.UpdateCursor(buffer_layer_dissolved, ["PID", OID_Field]) as cursor:
            for row in cursor:
                row[0] = row[1]
                cursor.updateRow(row)
        del cursor

        input_buffer_join = self.create_temp_layer('input_buffer_join')
        arcpy.SpatialJoin_analysis(input_layer_processed, buffer_layer_dissolved, input_buffer_join)
        # adding random values to all Polygons with buffer ID == 0:
        increment = 0
        with arcpy.da.UpdateCursor(input_buffer_join, ["PID"]) as incrementCursor:
            for row in incrementCursor:
                if row[0] is None:
                    increment -= 1
                    row[0] = increment
                incrementCursor.updateRow(row)
        del incrementCursor
        input_layer_connected = self.create_temp_layer('input_layer_connected')
        arcpy.Dissolve_management(input_buffer_join, input_layer_connected, "PID", [["PID", "COUNT"]])
        arcpy.JoinField_management(input_buffer_join, "PID", input_layer_connected, "PID", "COUNT_PID")

        # cleanup
        arcpy.Delete_management(buffer_layer)
        arcpy.Delete_management(buffer_erased_layer)
        arcpy.Delete_management(erased_singlepart_layer)
        arcpy.Delete_management(buffer_notBuffer_join)
        arcpy.Delete_management(buffer_notBuffer_joinFC)
        arcpy.Delete_management(buffer_notBuffer_join_selected)
        arcpy.Delete_management(buffer_layer_dissolved)
        arcpy.Delete_management(buffer_notBuffer_joinFC)

        return input_buffer_join, input_layer_connected

    # def __interpatch(self, inAreas):
    #    ## create subset of combinations
    #    ## [x,y,z] => [(x,y),(x,z),(y,z)]
    #    subset = list(combinations(inAreas, 2))
    #    ## multiply each combination element with each other and with 2:
    #    ## [(x*y*2),(x*z*2),(y*z*2)]
    #    subset_inter = [x[0] * x[1] * 2 for x in subset]
    #    return subset_inter

    def execute(self, parameters, messages):
        setup_debug_mode()
        arcpy.SetLogHistory(True)
        # getting params
        input_parameters = ScriptParameters(parameters)
        input_area = input_parameters.in_area.valueAsText
        stat_layer = input_parameters.stat_layer.valueAsText
        road_layer = input_parameters.road_layer.valueAsText
        settlement_layer = input_parameters.settlement_layer.valueAsText
        buffer_size = input_parameters.buffer_size.value
        line_buffer_size = input_parameters.line_buffer_size.value
        classFieldName = input_parameters.class_field.valueAsText
        class_field = get_field(input_area, classFieldName)
        class_list = getParameterValues(input_parameters.class_list)

        self.prepare_stat_layer(stat_layer,
                                EnsureUnitIDFieldExists(),
                                EnsureFieldExists(self.cbi_field, "DOUBLE", 0),
                                EnsureFieldExists(self.totalArea_field, "DOUBLE", 0),
                                EnsureFieldExists(self.interPatch_field, "DOUBLE", 0),
                                EnsureFieldExists(self.intraPatch_field, "DOUBLE", 0),
                                EnsureFieldExists(self.interStat_field, "DOUBLE", 0),
                                EnsureFieldExists(self.intraStat_field, "DOUBLE", 0),
                                EnsureFieldExists(self.cbi_stat_field, "DOUBLE", 0),
                                EnsureFieldExists(self.zoneArea_field, "DOUBLE", 0)
                                )

        barrier_layer, roads_bufferd = self.generate_barrierLayer_CBI(road_layer, line_buffer_size, settlement_layer,
                                                                      stat_layer)
        input_layer_processed = self.prepare_InputLayer_CBI(input_area, stat_layer, class_field,
                                                            class_list, roads_bufferd)
        input_buffer_join, input_layer_connected = self.generateBuffer_CBI(input_layer_processed, stat_layer,
                                                                           barrier_layer, buffer_size)

        msg = "Calculate Connectivities"
        log(msg)
        arcpy.SetProgressor("step", msg, 0, int(arcpy.GetCount_management(stat_layer).getOutput(0)), 1)

        ####calculate cbi and update statistic lyr:
        try:
            stat_layer_fields = [UNIT_ID_FIELD_NAME,
                                 "SHAPE@AREA",
                                 self.cbi_field,  # row[2] in cur
                                 self.totalArea_field,  # row[3]
                                 self.interPatch_field,  # row[4]
                                 self.intraPatch_field,  # row[5]
                                 self.intraStat_field,  # row[6]
                                 self.interStat_field,  # row[7]
                                 self.cbi_stat_field,  # row[8]
                                 self.zoneArea_field  # row[9]
                                 ]
            stat_zone_area = 0

            with intersect_analyzed_with_stat_layer(stat_layer, input_buffer_join) as nonDiss_Intersection, \
                    UpdateCursor(stat_layer, stat_layer_fields) as cur:
                for row in cur:
                    arcpy.SetProgressorPosition()
                    try:
                        where = "%s=%s" % (UNIT_ID_FIELD_NAME, row[0])
                        area_list_intra = []
                        area_list_groups = []

                        #  get current zone area (stat layer):
                        with SearchCursor(stat_layer, ("SHAPE@AREA"), where) as statCur:
                            for statRow in statCur:
                                stat_zone_area = statRow[0]
                        log("Zone Area in Unit %s = " % (row[0]) + str(stat_zone_area))
                        # get areas of all features no matter if in a join or not:
                        with SearchCursor(nonDiss_Intersection, ("SHAPE@AREA"), where) as cur2:
                            for area in cur2:
                                area_list_intra.append(area[0])

                            ## calculating intra-patch-connectivity
                            areaTotal = sum(area for area in area_list_intra)
                            log("AreaTotal in Unit %s = " % (row[0]) + str(areaTotal))
                            try:
                                log("Calculating Intrapatch Connectivity")
                                intra_patch_connectivity = (sum(
                                    (math.pow(area, 2)) for area in area_list_intra)) * 0.0001 / areaTotal
                                intra_stat_connectivity = (sum(
                                    (math.pow(area, 2)) for area in area_list_intra)) * 0.0001 / stat_zone_area
                            except ZeroDivisionError:
                                intra_patch_connectivity = 0
                                intra_stat_connectivity = 0

                            row[5] = intra_patch_connectivity
                            row[6] = intra_stat_connectivity
                            row[3] = areaTotal
                            row[9] = stat_zone_area

                        with intersect_analyzed_with_stat_layer(stat_layer, input_layer_connected) as diss_Intersection:
                            with SearchCursor(diss_Intersection, ("SHAPE@AREA"),
                                              where) as groups:  # all features which are joined with other features
                                for area in groups:
                                    area_list_groups.append(area[0])

                        # delete cursors/arrays/variables to save RAM:
                        del statCur, cur2, area_list_intra

                        total_patch_connectivity = (sum(
                            (math.pow(group_area, 2)) for group_area in area_list_groups)) * 0.0001 / areaTotal
                        total_stat_connectivity = (sum(
                            (math.pow(group_area, 2)) for group_area in area_list_groups)) * 0.0001 / stat_zone_area

                        inter_patch_connectivity = total_patch_connectivity - intra_patch_connectivity
                        inter_stat_connectivity = total_stat_connectivity - intra_stat_connectivity

                        row[2] = total_patch_connectivity
                        row[8] = total_stat_connectivity
                        row[7] = inter_stat_connectivity
                        row[4] = inter_patch_connectivity

                        cur.updateRow(row)

                        log("InterPatch Connectivity in Unit %s = " % (row[0]) + str(row[4]))
                        log("IntraPatch Connectivity in Unit %s = " % (row[0]) + str(row[5]))
                        log("CBI IND2 in Unit %s = " % (row[0]) + str(row[2]))
                    except Exception as e:
                        arcpy.AddError('Error: ' + str(e))
        ##
        finally:
            arcpy.ResetProgressor()


class InterpersionJuxtaposition(MetricsCalcTool):
    fields = {}
    _classes_template = {}
    iji_field = 'iji'

    def __init__(self):
        MetricsCalcTool.__init__(self)
        self.label = "Interpersion Juxtaposition Index"
        self.description = '''Calculates Interpersion Juxtaposition Index for each statistical zone'''

    def prepare(self):
        params = MetricsCalcTool.getParameterInfo(self)
        return params

    def execute(self, parameters, messages):
        setup_debug_mode()
        input_parameters = ScriptParameters(parameters)
        input_area = input_parameters.in_area.valueAsText
        stat_layer = input_parameters.stat_layer.valueAsText
        classFieldName = input_parameters.class_field.valueAsText
        classField = get_field(input_area, classFieldName)
        classList = getParameterValues(input_parameters.class_list)

        self.prepare_stat_layer(stat_layer,
                                EnsureUnitIDFieldExists(),
                                EnsureFieldExists(self.iji_field, "DOUBLE", 0),
                                )

        msg = "Preparing for IJI"
        log(msg)
        arcpy.SetProgressor("step", msg, 0, int(arcpy.GetCount_management(stat_layer).getOutput(0)), 1)

        edgesLayerName = "in_memory\\inputEdges%d" % random.randint(0, 1000000)
        edgesLayerNameLyr = edgesLayerName + 'Layer'
        neighborsName = "in_memory\\neighborhoodAnalysis%d" % random.randint(0, 1000000)
        neighborsNameLyr = neighborsName + 'Layer'

        try:
            arcpy.MakeFeatureLayer_management(input_area, edgesLayerNameLyr)
            arcpy.PolygonNeighbors_analysis(edgesLayerNameLyr, neighborsNameLyr, ('FID', classFieldName))

            stat_layer_fields = [UNIT_ID_FIELD_NAME,
                                 "SHAPE@AREA",  # = row[2]
                                 self.iji_field]  # = row[3]

            with intersect_analyzed_with_stat_layer(stat_layer, edgesLayerNameLyr, class_field=classField,
                                                    class_list=classList) as edgesIntersection, \
                    UpdateCursor(stat_layer, stat_layer_fields) as cur:

                for row in cur:
                    arcpy.SetProgressorPosition()
                    try:
                        edgesIntersection_fields = [f.name for f in arcpy.ListFields(edgesIntersection)]
                        edges_length = 0
                        edges_count = 0
                        edge_length_list = []
                        with SearchCursor(edgesIntersection, ("FID_" + edgesIntersection[10:] + "_selected"),
                                          """"unitID" = %s""" % (row[0])) as cur2:
                            for cur2_row in cur2:
                                with SearchCursor(neighborsNameLyr,
                                                  ("LENGTH", "src_" + classFieldName, "nbr_" + classFieldName),
                                                  """"src_FID" = %s""" % (
                                                          cur2_row[0] - 1)) as cur3:  # cur2 = list of neighbor-edges
                                    for cur3_row in cur3:
                                        if (cur3_row[0] != 0):
                                            edge_length_list.append((cur3_row[0], cur3_row[1], cur3_row[2]))

                        # sort data in respective class-combinations and eliminate duplicate edges
                        sort = [(x[0], sorted((x[1], x[2]))) for x in edge_length_list]
                        distinct_edge_length_list = [list(v) for _, v in
                                                     groupby(sorted(sort, key=itemgetter(1)), itemgetter(1))]

                        edges_count_list = []
                        for i in distinct_edge_length_list:
                            edges_length += sum(j[0] for j in i)
                            for k in i:
                                edges_count_list.append(k[1])
                        edges_count = len(set([item for sublist in edges_count_list for item in sublist]))

                        ##       calculate iji here.........
                        numerator_iji = 0
                        for cl_combination in distinct_edge_length_list:
                            numerator_iji += ((float(sum(j[0] for j in cl_combination) / edges_length) * math.log(
                                (float(sum(j[0] for j in cl_combination) / edges_length)))))

                        if (edges_count > 1):
                            try:
                                iji = (numerator_iji * (-1)) / (math.log((0.5 * (edges_count * (edges_count - 1)))))
                            except ZeroDivisionError:  # because math.log(1) = 0
                                iji = 1
                        else:
                            iji = 0
                        row[2] = iji * 100  # update field with iji (in %) value
                        log("IJI in Unit %s = " % (row[0]) + str(row[2]))
                        cur.updateRow(row)
                    except Exception as e:
                        arcpy.AddError('Error: ' + str(e))
        finally:
            # cleanup
            arcpy.Delete_management(neighborsName)
            arcpy.Delete_management(neighborsNameLyr)
            arcpy.Delete_management(edgesLayerName)
            arcpy.Delete_management(edgesLayerNameLyr)
        arcpy.ResetProgressor()


class EffectiveMeshSize(MetricsCalcTool):
    # todo: remove params to one input layer
    fields = {}
    _classes_template = {}
    totalArea_field = 'areaTotal'

    def __init__(self):
        MetricsCalcTool.__init__(self)
        self.label = "Effective Mesh Size"
        self.description = '''Calculates Effective Mesh Size for each statistical zone'''

    def getParameterInfo(self):
        optional_input_processed = arcpy.Parameter(
            displayName="Processed Input Layer",
            name="optional_input_processed",
            datatype="GPFeatureLayer",
            parameterType="Optional",
            direction="Input"
        )

        optional_input_unprocessed = arcpy.Parameter(
            displayName="Unprocessed Input Layer",
            name="optional_input_unprocessed",
            datatype="GPFeatureLayer",
            parameterType="Optional",
            direction="Input"
        )

        calcMethod = arcpy.Parameter(
            displayName="Calculation Method",
            name="calcMethod",
            datatype="GPString",
            parameterType="Required",
            direction="Input",
            multiValue=False)

        calc_method_list = ['Cut Method', 'CBC Method']

        checkbox_preprocess_layers = arcpy.Parameter(
            displayName="Input Layer is not preprocessed? (optional)",
            name="checkbox_preprocess_layers",
            datatype="GPBoolean",
            parameterType="Optional",
            direction="Input"
        )

        checkbox_line_features = arcpy.Parameter(
            displayName="Erase line features (like roads)? (optional)",
            name="checkbox_line_features",
            datatype="GPBoolean",
            parameterType="Optional",
            direction="Input"
        )

        checkbox_polygon_features = arcpy.Parameter(
            displayName="Erase polygon features (like settlements)? (optional)",
            name="checkbox_polygon_features",
            datatype="GPBoolean",
            parameterType="Optional",
            direction="Input"
        )

        road_layer = arcpy.Parameter(
            displayName="Road Layer",
            name="road_layer",
            datatype="GPFeatureLayer",
            parameterType="Optional",
            direction="Input",
            multiValue=True)

        buffer_size = arcpy.Parameter(
            displayName="Line Buffer Size",
            name="buffer_size",
            datatype="GPDouble",
            parameterType="Required",
            direction="Input")
        buffer_size.value = 0.5

        settlement_layer = arcpy.Parameter(
            displayName="Settlement Layer",
            name="settlement_layer",
            datatype="GPFeatureLayer",
            parameterType="Optional",
            direction="Input",
            multiValue=True)

        calcMethod.filter.type = 'ValueList'
        calcMethod.filter.list = calc_method_list

        params = MetricsCalcTool.getParameterInfo(self)
        # remove class params (smarter way than recreating i/o-Params
        native_zonalmetrics_params = [optional_input_processed] + params[1:2] + params[4:]
        return native_zonalmetrics_params + [calcMethod, checkbox_preprocess_layers, optional_input_unprocessed,
                                             checkbox_line_features, road_layer, buffer_size,
                                             checkbox_polygon_features, settlement_layer]

    def updateParameters(self, params):
        MetricsCalcTool.updateParameters(self, params)
        parameters = ScriptParameters(params)

        '''only enable barrier params if checkbox is checked'''
        checkbox_preprocessing_param = parameters.checkbox_preprocess_layers
        checkbox_erase_lines_param = parameters.checkbox_line_features
        checkbox_erase_polygons_param = parameters.checkbox_polygon_features
        optional_processed_input_param = parameters.optional_input_processed
        optional_unprocessed_input_param = parameters.optional_input_unprocessed
        road_layer_param = parameters.road_layer
        buffer_size_param = parameters.buffer_size
        settlement_layer_param = parameters.settlement_layer

        '''activate barrier dialog'''
        if checkbox_preprocessing_param.value == True:
            optional_unprocessed_input_param.enabled = True
            optional_processed_input_param.enabled = False
            checkbox_erase_lines_param.enabled = True
            checkbox_erase_polygons_param.enabled = True
            # erase roads
            if checkbox_erase_lines_param.value == True:
                road_layer_param.enabled = True
                # buffer size for roads
                buffer_size_param.enabled = True
            else:
                road_layer_param.enabled = False
                buffer_size_param.enabled = False
            # erase settlements
            if checkbox_erase_polygons_param.value == True:
                settlement_layer_param.enabled = True
            else:
                settlement_layer_param.enabled = False
        else:
            # todo: check if this is redundand
            checkbox_erase_lines_param.enabled = False
            checkbox_erase_polygons_param.enabled = False
            optional_unprocessed_input_param.enabled = False
            optional_processed_input_param.enabled = True
            road_layer_param.enabled = False
            buffer_size_param.enabled = False
            settlement_layer_param.enabled = False

        return

    def updateMessages(self, params):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""

        parameters = ScriptParameters(params)
        checkbox_preprocessing_param = parameters.checkbox_preprocess_layers
        checkbox_erase_polygons_param = parameters.checkbox_polygon_features
        checkbox_erase_lines_param = parameters.checkbox_line_features

        optional_processed_input_param = parameters.optional_input_processed
        optional_unprocessed_input_param = parameters.optional_input_unprocessed
        checkbox_erase_polygons_param = parameters.checkbox_polygon_features
        checkbox_erase_lines_param = parameters.checkbox_line_features

        # optional_processed_input_param.clearMessage()

        """Because parameters are read-only, you can only set an error message.
        Following construction forces the user to enter an input layer (either unprocessed or processed"""
        if checkbox_preprocessing_param.altered:
            if checkbox_erase_polygons_param.altered or checkbox_erase_lines_param.altered:
                if not optional_unprocessed_input_param.altered:
                    optional_unprocessed_input_param.setErrorMessage("Input Layer is required")
                else:
                    optional_unprocessed_input_param.clearMessage()
            elif not checkbox_preprocessing_param.value:
                if not optional_processed_input_param.altered:
                    optional_processed_input_param.setErrorMessage("Input Layer is required")
        return

    def prepare_input_layer_meff(self, input_area):
        log("Preparing input layer")
        OID_Field = arcpy.Describe(input_area).OIDFieldName
        input_layer_selected = self.create_temp_layer('inputLayer_selected')
        arcpy.CopyFeatures_management(input_area, input_layer_selected)
        arcpy.AddField_management(input_layer_selected, "ID_element", "LONG")
        with arcpy.da.UpdateCursor(input_layer_selected, ["ID_element", OID_Field]) as cursor:
            for row in cursor:
                row[0] = row[1]
                cursor.updateRow(row)

        return input_layer_selected

    def generate_barrier_layer_meff(self, road_layers, settlement_layers, line_buffer_size):
        log("Preparing barrier layer")
        # merging multivalue layers into one

        if settlement_layers:
            settlement_layer = self.create_temp_layer('settlement_layer')
            arcpy.Merge_management(settlement_layers, settlement_layer)
        '''Line Layer processing'''
        roads_buffered = []
        if road_layers:
            road_layer = self.create_temp_layer('road_layer')
            arcpy.Merge_management(road_layers, road_layer)
            roads_buffered = self.create_temp_layer('roads_buffered')
            arcpy.Buffer_analysis(road_layer, roads_buffered, line_buffer_size, "FULL", "ROUND", "ALL")

        '''Merge existing barrier layers (lines and polygons) if more than one layer. 
        If no barrier layer -> nothing to merge'''

        barriers = [roads_buffered] + settlement_layers
        barrier_layer = self.create_temp_layer('barrier_layer')
        if len(barriers) > 1:
            arcpy.Merge_management(barriers, barrier_layer)
        elif len(barriers) == 1:
            barrier_layer = barriers[0]

        return barrier_layer

    def erase_barriers(self, input_layer_selected, barrier_layer):
        log("Generating Connectivities")
        input_erased_layer = self.create_temp_layer('input_erased_layer')
        arcpy.Erase_analysis(input_layer_selected, barrier_layer, input_erased_layer)

        return input_erased_layer

    def execute(self, parameters, messages):
        setup_debug_mode()
        arcpy.SetLogHistory(True)
        # getting params
        input_parameters = ScriptParameters(parameters)
        input_area = input_parameters.optional_input_processed.valueAsText
        # If input is a unprocessed layer
        if input_area is None:
            input_area = input_parameters.optional_input_unprocessed.valueAsText

        stat_layer = input_parameters.stat_layer.valueAsText
        '''Parse Multivalue Inputs'''
        road_layer_multiValue = input_parameters.road_layer.valueAsText
        settlement_layer_multiValue = input_parameters.settlement_layer.valueAsText
        if input_parameters.buffer_size.value != 0:
            line_buffer_size = input_parameters.buffer_size.value
        else:
            line_buffer_size = 0
        road_layers = []
        settlement_layers = []
        if road_layer_multiValue is not None:
            road_layers = road_layer_multiValue.split(";")
        if settlement_layer_multiValue is not None:
            settlement_layers = settlement_layer_multiValue.split(";")

        calcMethod_value = input_parameters.calcMethod.valueAsText

        if calcMethod_value == "Cut Method":
            mEff_field = "mEff_CUT"
        else:
            mEff_field = "meff_CBC"

        MetricsCalcTool.prepare_stat_layer(stat_layer,
                                           EnsureUnitIDFieldExists(),
                                           EnsureFieldExists(mEff_field, "DOUBLE", 0),
                                           EnsureFieldExists(self.totalArea_field, "DOUBLE", 0)
                                           )

        input_layer_selected = self.prepare_input_layer_meff(input_area)

        if (road_layers or settlement_layers):
            barrier_layer = self.generate_barrier_layer_meff(road_layers, settlement_layers, line_buffer_size)
            input_layer = self.erase_barriers(input_layer_selected, barrier_layer)
        else:
            input_layer = input_layer_selected

        msg = "Calculate mEff"
        log(msg)
        arcpy.SetProgressor("step", msg, 0, int(arcpy.GetCount_management(stat_layer).getOutput(0)), 1)

        ####calculate cbd and update statistic lyr:
        try:
            stat_layer_fields = [UNIT_ID_FIELD_NAME,
                                 "SHAPE@AREA",
                                 mEff_field,  # row[2] in cur
                                 self.totalArea_field,  # row[3]
                                 ]
            areaTotal = 0
            stat_zone_area = 0
            with intersect_analyzed_with_stat_layer(stat_layer, input_layer) as Intersection, \
                    UpdateCursor(stat_layer, stat_layer_fields) as cur:
                for row in cur:
                    where = "%s=%s" % (UNIT_ID_FIELD_NAME, row[0])
                    arcpy.SetProgressorPosition()
                    try:
                        #  get current Area_Total (stat layer):
                        with arcpy.da.SearchCursor(stat_layer, ("SHAPE@AREA"), where) as statCur:
                            for statRow in statCur:
                                stat_zone_area = statRow[0]
                        log("Zone Area in Unit %s = " % (row[0]) + str(stat_zone_area))

                        areaList = []
                        with arcpy.da.SearchCursor(Intersection, ("SHAPE@AREA", "ID_element"), where) as intersectCur:
                            for intersectCurRow in intersectCur:
                                # get Features with same ID from (unclipped) input_layer
                                where_ID = "%s=%s" % ("ID_element", intersectCurRow[1])
                                with arcpy.da.SearchCursor(input_layer, ("SHAPE@AREA"), where_ID) as inputClippedCur:
                                    for inputClippedCurRow in inputClippedCur:
                                        # multiply every feature-area of intersected layer with its complete area (CBC) cf. Moser et al 2007
                                        area_sq = intersectCurRow[0] * intersectCur[0]
                                        area_cmpl_sq = intersectCurRow[0] * inputClippedCurRow[0]
                                        if (calcMethod_value == "Cut Method"):
                                            areaList.append(area_sq)
                                        else:
                                            areaList.append(area_cmpl_sq)
                            #mEff = (sum(area for area in areaList)) * 0.0001 / stat_zone_area  # in ha
                            mEff = (sum(area for area in areaList)) * 0.000001 / stat_zone_area  # in km2
                            row[3] = stat_zone_area
                            row[2] = mEff

                            log("mEff in Unit %s = " % (row[0]) + str(row[2]))
                        cur.updateRow(row)
                    except Exception as e:
                        arcpy.AddError('Error: ' + str(e))
        finally:
            arcpy.ResetProgressor()


class ProximityIndex(MetricsCalcTool):
    # todo: remove params to one input layer
    fields = {}
    _classes_template = {}
    prox_field = 'PXfg'
    totalArea_field = 'areaTotal'

    def __init__(self):
        MetricsCalcTool.__init__(self)
        self.label = "Proximity Index"
        self.description = '''Calculates Proximity Index for each statistical zone'''

    def getParameterInfo(self):
        road_layer = arcpy.Parameter(
            displayName="Line Barrier Layer (like roads)",
            name="road_layer",
            datatype="GPFeatureLayer",
            parameterType="Optional",
            direction="Input",
            category="Barriers",
            multiValue=True)

        line_buffer_size = arcpy.Parameter(
            displayName="Line Buffer Size",
            name="line_buffer_size",
            datatype="GPDouble",
            parameterType="Required",
            category="Barriers",
            direction="Input")
        line_buffer_size.value = 0.5

        buffer_size = arcpy.Parameter(
            displayName="Buffer Size",
            name="buffer_size",
            datatype="GPDouble",
            parameterType="Required",
            direction="Input")
        buffer_size.value = 50

        settlement_layer = arcpy.Parameter(
            displayName="Polygon Barrier Layer (like settlements)",
            name="settlement_layer",
            datatype="GPFeatureLayer",
            parameterType="Optional",
            category="Barriers",
            direction="Input",
            multiValue=True)

        out_workspace = arcpy.Parameter(
            displayName="Workspace for PXfg-Single-Patches",
            name="out_workspace",
            datatype="Workspace",
            parameterType="Optional",
            direction="Input")

        checkbox_sqared_implementation = arcpy.Parameter(
            displayName="Sqare distances? (optional)",
            name="checkbox_sqared_implementation",
            datatype="GPBoolean",
            parameterType="Optional",
            direction="Input"
        )

        params = MetricsCalcTool.getParameterInfo(self)
        # remove class params (smarter way than recreating i/o-Params
        params_without_class = params[:2] + params[4:]
        return params_without_class + [buffer_size, checkbox_sqared_implementation,
                                       road_layer, line_buffer_size,
                                       settlement_layer, out_workspace]

    def updateParameters(self, params):
        MetricsCalcTool.updateParameters(self, params)
        parameters = ScriptParameters(params)

        return

    def prepareInputLayerPXfg(self, input_area):
        log("Preparing input layer")
        OID_Field = arcpy.Describe(input_area).OIDFieldName
        input_layer_selected = self.create_temp_layer('inputLayer_selected')
        arcpy.CopyFeatures_management(input_area, input_layer_selected)
        arcpy.AddField_management(input_layer_selected, "ID_element", "LONG")
        with arcpy.da.UpdateCursor(input_layer_selected, ["ID_element", OID_Field]) as cursor:
            for row in cursor:
                row[0] = row[1]
                cursor.updateRow(row)

        return input_layer_selected

    def generateBarrierLayerPXfg(self, road_layers, settlement_layers, line_buffer_size):
        log("Preparing barrier layer")
        # merging multivalue layers into one

        if settlement_layers:
            settlement_layer = self.create_temp_layer('settlement_layer')
            arcpy.Merge_management(settlement_layers, settlement_layer)
        '''Line Layer processing'''
        roads_buffered = []
        if road_layers:
            road_layer = self.create_temp_layer('road_layer')
            arcpy.Merge_management(road_layers, road_layer)
            roads_buffered = self.create_temp_layer('roads_buffered')
            arcpy.Buffer_analysis(road_layer, roads_buffered, line_buffer_size, "FULL", "ROUND", "ALL")

        '''Merge existing barrier layers (lines and polygons) if more than one layer. 
        If no barrier layer -> nothing to merge'''

        barriers = [roads_buffered] + settlement_layers
        barrier_layer = self.create_temp_layer('barrier_layer')
        if len(barriers) > 1:
            arcpy.Merge_management(barriers, barrier_layer)
        elif len(barriers) == 1:
            barrier_layer = barriers[0]

        return barrier_layer

    def prepareForNeighborhoodAnalysis(self, input_layer, stat_layer, barrier_layer, buffer_size):
        log("Preparing for patch neighborhood analysis")
        input_buffered = self.create_temp_layer('input_layer_buffered')
        arcpy.Buffer_analysis(input_layer, input_buffered, buffer_size, "FULL", "ROUND", "NONE")
        '''Repair broken geometry somehow helps with 999999 bug of ArcGIS Pro'''
        arcpy.RepairGeometry_management(input_buffered)

        buffers_clipped_zone = self.create_temp_layer('buffers_clipped_zone')
        arcpy.Clip_analysis(input_buffered, stat_layer, buffers_clipped_zone)
        buffer_erased_layer = self.create_temp_layer('buffers_erased_layer')
        '''Erase barriers'''
        # todo: Überprüfen ob die Zwischenschritte rauskönnen
        if barrier_layer:
            arcpy.Erase_analysis(buffers_clipped_zone, barrier_layer, buffer_erased_layer)
        else:
            buffer_erased_layer = buffers_clipped_zone
        erased_singlepart_layer = self.create_temp_layer("erased_single")
        arcpy.MultipartToSinglepart_management(buffer_erased_layer, erased_singlepart_layer)

        '''Spatial join to eliminate all fake connections:'''
        buffer_notBuffer_join = self.create_temp_layer('buf_nBuf_join')
        arcpy.SpatialJoin_analysis(erased_singlepart_layer, input_layer, buffer_notBuffer_join, "JOIN_ONE_TO_MANY",
                                   "KEEP_COMMON", "", "INTERSECT")

        buffer_notBuffer_joinFC = self.create_temp_layer('buffer_nBuf_join_FC')
        arcpy.MakeFeatureLayer_management(buffer_notBuffer_join, buffer_notBuffer_joinFC)
        arcpy.SelectLayerByAttribute_management(buffer_notBuffer_joinFC, "NEW_SELECTION", ' "ID_element" = "JOIN_FID" ')
        buffer_notBuffer_join_selected = self.create_temp_layer('buf_nBuf_join_selected')
        arcpy.CopyFeatures_management(buffer_notBuffer_joinFC, buffer_notBuffer_join_selected)

        '''Spatial Join to get all feature combinations'''
        input_buffer_SJ = self.create_temp_layer('input_buffer_SJ')
        arcpy.SpatialJoin_analysis(input_layer, buffer_notBuffer_join_selected, input_buffer_SJ, "JOIN_ONE_TO_MANY",
                                   "KEEP_COMMON", "", "INTERSECT")

        '''Select Features and copy them to new temp FC'''
        select_features_for_analysis = self.create_temp_layer('select_features_for_analysis')
        arcpy.MakeFeatureLayer_management(input_buffer_SJ, select_features_for_analysis)
        arcpy.SelectLayerByAttribute_management(select_features_for_analysis, "NEW_SELECTION",
                                                ' "JOIN_FID" <> "ID_element" ')
        prepared_input_layer = self.create_temp_layer('prepared_input_layer')
        arcpy.CopyFeatures_management(select_features_for_analysis, prepared_input_layer)
        return prepared_input_layer

    def execute(self, parameters, messages):
        setup_debug_mode()
        arcpy.SetLogHistory(True)
        # getting params
        input_parameters = ScriptParameters(parameters)
        input_area = input_parameters.in_area.valueAsText
        stat_layer = input_parameters.stat_layer.valueAsText
        out_workspace = input_parameters.out_workspace.valueAsText
        checkbox_sqared_implementation = input_parameters.checkbox_sqared_implementation.value

        '''Parse Multivalue Inputs'''
        road_layer_multiValue = input_parameters.road_layer.valueAsText
        settlement_layer_multiValue = input_parameters.settlement_layer.valueAsText
        if input_parameters.line_buffer_size.value != 0:
            line_buffer_size = input_parameters.line_buffer_size.value
        else:
            line_buffer_size = 0
        if input_parameters.buffer_size.value != 0:
            buffer_size = input_parameters.buffer_size.value
        else:
            buffer_size = 0

        road_layers = []
        settlement_layers = []
        if road_layer_multiValue is not None:
            road_layers = road_layer_multiValue.split(";")
        if settlement_layer_multiValue is not None:
            settlement_layers = settlement_layer_multiValue.split(";")

        MetricsCalcTool.prepare_stat_layer(stat_layer,
                                           EnsureUnitIDFieldExists(),
                                           EnsureFieldExists(self.prox_field, "DOUBLE", 0)
                                           )

        input_layer_selected = self.prepareInputLayerPXfg(input_area)

        barrier_layer = self.generateBarrierLayerPXfg(road_layers, settlement_layers, line_buffer_size)
        '''input_layer = self.__eraseBarriers(input_layer_selected, barrier_layer)'''
        neighborhood_analysis_layer = self.prepareForNeighborhoodAnalysis(input_layer_selected, stat_layer,
                                                                          barrier_layer, buffer_size)

        msg = "Calculate pxFG"
        log(msg)
        arcpy.SetProgressor("step", msg, 0, int(arcpy.GetCount_management(stat_layer).getOutput(0)), 1)

        '''Calculations'''
        try:
            stat_layer_fields = [UNIT_ID_FIELD_NAME,
                                 "SHAPE@AREA",
                                 self.prox_field,  # row[2] in cur
                                 ]

            '''Add SUM_PXfg field to input layer'''
            arcpy.AddField_management(input_layer_selected, "SUM_PXfg", "DOUBLE")
            with intersect_analyzed_with_stat_layer(stat_layer, neighborhood_analysis_layer) as Intersection, \
                    UpdateCursor(stat_layer, stat_layer_fields) as cur:
                for row in cur:
                    log("Calculating PXfg for unit: = %s" % row[0])
                    prox_single_values = []  # array to collect single ratios for each neighbor patch (distance/area)
                    where = "%s=%s" % (UNIT_ID_FIELD_NAME, row[0])
                    arcpy.SetProgressorPosition()
                    try:
                        feature_prox_values = []
                        with arcpy.da.SearchCursor(Intersection,
                                                   ("SHAPE@AREA", "SHAPE@", "JOIN_FID"),
                                                   where) \
                                as intersectCur:
                            for intersectCurRow in intersectCur:
                                # area_target_feature = intersectCurRow[0]
                                geom_target_feature = intersectCurRow[1]
                                ''' get Features with from input layer where OID = ID_element '''
                                where_ID = "%s=%s" % ("ID_element", intersectCurRow[2])
                                with arcpy.da.SearchCursor(input_layer_selected, ("SHAPE@AREA", "SHAPE@", "ID_element"),
                                                           where_ID) as inputFeaturesCur:
                                    for inputFeaturesCurRow in inputFeaturesCur:
                                        area_orig_feature = inputFeaturesCurRow[0]
                                        geom_orig_feature = inputFeaturesCurRow[1]
                                        '''Calculate distance between each original to target patch'''
                                        dist_patches = geom_orig_feature.distanceTo(geom_target_feature)
                                        if dist_patches != 0:
                                            if checkbox_sqared_implementation:
                                                log("Distances get squared")
                                                prox_single_value = area_orig_feature / math.pow(dist_patches, 2)
                                            else:
                                                prox_single_value = area_orig_feature / dist_patches

                                            feature_prox_values.append((inputFeaturesCurRow[2], prox_single_value))
                                            '''Write PX value into input layer'''
                                        else:
                                            prox_single_value = 0

                                        prox_single_values.append(prox_single_value)

                        pxFG = (sum(val for val in prox_single_values))
                        row[2] = pxFG
                        log("pxFG in Unit %s = " % (row[0]) + str(row[2]))
                        cur.updateRow(row)

                        '''Group and sum PXfg for every feature and store it in a dict'''
                        sorted_prox_values_by_id = {}
                        for t in feature_prox_values:
                            if t[0] in sorted_prox_values_by_id:
                                sorted_prox_values_by_id[t[0]] = sorted_prox_values_by_id[t[0]] + t[1]
                            else:
                                sorted_prox_values_by_id[t[0]] = t[1]

                        '''Update outputlayer with dict values'''
                        with arcpy.da.UpdateCursor(input_layer_selected,
                                                   ["SUM_PXfg", "ID_element"]) as input_cursor:
                            for input_row in input_cursor:
                                if input_row[1] in sorted_prox_values_by_id.keys():
                                    current_id_element = input_row[1]
                                    input_row[0] = sorted_prox_values_by_id[current_id_element]
                                    input_cursor.updateRow(input_row)
                                else:
                                    continue


                    except Exception as e:
                        arcpy.AddError('Error: ' + str(e))
        finally:
            arcpy.FeatureClassToFeatureClass_conversion(input_layer_selected, out_workspace, "SUM_PXfg")
            arcpy.ResetProgressor()


class UrbanSprawlMetrics(MetricsCalcTool):
    # todo: remove params to one input layer
    fields = {}
    _classes_template = {}
    dis_field = 'DIS'
    wdis_field = 'w1DIS'
    ts_field = 'TS'
    up_field = 'UP'
    ud_field = 'UD'
    wud_field = 'w2UD'
    wup_field = 'WUP'

    def __init__(self):
        MetricsCalcTool.__init__(self)
        self.label = "Urban Sprawl Metrics"
        self.description = '''Calculates WUP for each statistical zone'''

    def getParameterInfo(self):
        params = MetricsCalcTool.getParameterInfo(self)
        builtup_areas = arcpy.Parameter(
            displayName="Builtup Raster",
            name="builtup_areas",
            datatype="GPRasterLayer",
            parameterType="Optional",
            direction="Input"
        )

        number_of_inhabitants_jobs_field = arcpy.Parameter(
            displayName="Number of Inhabitants and Jobs Field",
            name="number_of_inhabitants_jobs_field",
            datatype="Field",
            parameterType="Optional",
            direction="Input")
        number_of_inhabitants_jobs_field.parameterDependencies = [params[1].name]

        horizon_of_perception = arcpy.Parameter(
            displayName="Horizon Of Perception",
            name="horizon_of_perception",
            datatype="GPDouble",
            parameterType="Required",
            direction="Input")
        horizon_of_perception.value = 2000

        return [builtup_areas, params[1], number_of_inhabitants_jobs_field, horizon_of_perception]

    def updateParameters(self, params):
        MetricsCalcTool.updateParameters(self, params)
        parameters = ScriptParameters(params)
        return

    def calculate_si_values(self, dist_arr, wcc):
        """Calculates SI-Values for input array of inter-patch distances like [d1,d2,...dn]"""
        si_value_calc = 0

        for d_ik in dist_arr:
            si_value_calc += (math.sqrt(2 * d_ik + 1) - 1)
        number_urban_cells_within_hp = len(dist_arr)
        si_value = (1 / number_urban_cells_within_hp) * (si_value_calc + wcc)

        return si_value, number_urban_cells_within_hp

    def calculate_weighted_dis(self, si_array):
        """ Calculating dispersion"""
        # delete all zero values for better
        n = len(si_array)

        dis = (1 / n) * sum(si_array)
        # weighted DIS (Jaeger & Schwick 2014:299)
        wdis = 0.5 + (math.exp(0.294432 * dis - 12.955) / (1 + math.exp(0.294432 * dis - 12.955)))
        return dis, wdis

    def calculate_ts(self, si_array, cell_size):
        """ Calculating total sprawl"""
        ts = math.pow(cell_size, 2) * sum(si_array)
        return ts

    def calculate_up(self, total_zone_area, ts):
        """ Calculating urban permeation"""
        up = 1 / total_zone_area * ts
        return up

    def calculate_weighted_utilisation_density(self, builtup_area, number_inhabitants_jobs):
        """ Calculating weighted UD"""
        # todo: find out if ud is in ha or km2
        builtup_area_km2 = builtup_area / 1000000
        ud = number_inhabitants_jobs / builtup_area_km2
        weighted_utilisation_density = math.exp(4.159 - 0.000613125 * ud) / (1 + math.exp(4.159 - 0.000613125 * ud))
        return ud, weighted_utilisation_density

    def execute(self, parameters, messages):
        setup_debug_mode()
        arcpy.SetLogHistory(True)
        # getting params
        input_parameters = ScriptParameters(parameters)
        builtup_areas = input_parameters.builtup_areas.valueAsText
        stat_layer = input_parameters.stat_layer.valueAsText
        number_of_inhabitants_jobs_field = input_parameters.number_of_inhabitants_jobs_field.valueAsText
        horizon_of_perception = input_parameters.horizon_of_perception.value

        '''Get Raster Properties (cellsize to declare wcc)'''
        raster_properties = arcpy.GetRasterProperties_management(builtup_areas, "CELLSIZEX")
        raster_cellsize = int(raster_properties.getOutput(0))
        wcc = get_wcc(raster_cellsize)
        log("wcc = %s" % wcc)

        # stat_layer = parameters.stat_layer.valueAsText

        MetricsCalcTool.prepare_stat_layer(stat_layer,
                                           EnsureUnitIDFieldExists(),
                                           EnsureFieldExists(self.dis_field, "DOUBLE", 0),
                                           EnsureFieldExists(self.wdis_field, "DOUBLE", 0),
                                           EnsureFieldExists(self.ts_field, "DOUBLE", 0),
                                           EnsureFieldExists(self.up_field, "DOUBLE", 0),
                                           EnsureFieldExists(self.ud_field, "DOUBLE", 0),
                                           EnsureFieldExists(self.wud_field, "DOUBLE", 0),
                                           EnsureFieldExists(self.wup_field, "DOUBLE", 0),
                                           )

        msg = "Calculate SI_Values"
        log(msg)
        # arcpy.SetProgressor("step", msg, 0, int(arcpy.GetCount_management(stat_layer).getOutput(0)), 1)

        '''Convert Builtup Raster to Point Feature Class and Select only 1-values'''
        builtup_points = self.create_temp_layer('builtup_points')
        arcpy.RasterToPoint_conversion(builtup_areas, builtup_points)
        builtup_points_copy = self.create_temp_layer('builtup_points_selected')
        arcpy.MakeFeatureLayer_management(builtup_points, builtup_points_copy)
        # Select points:
        arcpy.SelectLayerByAttribute_management(builtup_points_copy, "NEW_SELECTION", ' "grid_code" = 1 ')
        builtup_points_selected = self.create_temp_layer('buf_nBuf_join_selected')
        arcpy.CopyFeatures_management(builtup_points_copy, builtup_points_selected)
        # Add si_value field
        arcpy.AddField_management(builtup_points_selected, "si_value", "DOUBLE")

        number_point_feature = int(arcpy.GetCount_management(builtup_points_selected).getOutput(0))
        arcpy.SetProgressor("step", "Processing Features...",
                            0, number_point_feature, 1)
        progressor = 0

        '''Calculate the SI_Values'''
        with arcpy.da.UpdateCursor(builtup_points_selected, ["SHAPE@", "si_value"]) as si_update_cursor:
            start_time = time.time()
            for point in si_update_cursor:
                progress_percentage = round((progressor / number_point_feature) * 100, 2)
                arcpy.SetProgressorLabel("Processed {0} of {1} Features ... {2} %".format(progressor,
                                                                                          number_point_feature,
                                                                                          progress_percentage))
                near_table = self.create_temp_layer('near_table')
                arcpy.GenerateNearTable_analysis(point[0], builtup_points_selected, near_table,
                                                 horizon_of_perception, 'NO_LOCATION', 'NO_ANGLE',
                                                 'ALL', horizon_of_perception)
                distances = []
                with arcpy.da.SearchCursor(near_table, "NEAR_DIST") as near_cursor:
                    for row in near_cursor:
                        distances.append(row[0])

                si_value, number_urban_cells_within_hp = self.calculate_si_values(distances, wcc)
                # log("si_value = %s" % si_value)
                # log("distances = %s" %(str(distances)))

                point[1] = si_value
                si_update_cursor.updateRow(point)
                arcpy.SetProgressorPosition()
                progressor += 1
        del si_update_cursor, near_table
        log("--- Si-value computation took %s seconds ---" % (time.time() - start_time))

        arcpy.ResetProgressor()

        '''Calculate WUP metrics for each statistical zone'''
        try:
            stat_layer_fields = [UNIT_ID_FIELD_NAME,
                                 "SHAPE@AREA",
                                 number_of_inhabitants_jobs_field,
                                 self.dis_field,  # row[3] in cur
                                 self.wdis_field,  # row[4]
                                 self.ts_field,  # row[5]
                                 self.up_field,  # row[6]
                                 self.ud_field,  # row[7]
                                 self.wud_field,  # row[8]
                                 self.wup_field  # row[9]
                                 ]

            with intersect_analyzed_with_stat_layer(stat_layer, builtup_points_selected) as Intersection, \
                    UpdateCursor(stat_layer, stat_layer_fields) as cur:
                for row in cur:
                    si_value_arr = []
                    builtup_area = 0
                    where = "%s=%s" % (UNIT_ID_FIELD_NAME, row[0])
                    arcpy.SetProgressorPosition()
                    arcpy.SetProgressorLabel("Calculating Metrics")
                    try:
                        with arcpy.da.SearchCursor(Intersection,
                                                   ("SHAPE@AREA", "si_value"),
                                                   where) \
                                as intersectCur:
                            for intersectCurRow in intersectCur:
                                # For each point add cellsize² to builtup area (the builtup area is needed for UD calculation)
                                builtup_area += math.pow(raster_cellsize, 2)
                                si_value_arr.append(intersectCurRow[1])

                        log("Urban Area = %s" % builtup_area)
                        zone_area = 0
                        weighted_ud = 0
                        OID_Field = arcpy.Describe(stat_layer).OIDFieldName
                        zone_where_clause = "%s=%s" % (OID_Field, row[0])
                        with arcpy.da.SearchCursor(stat_layer, ["SHAPE@AREA", number_of_inhabitants_jobs_field],
                                                   zone_where_clause) as stat_cursor:
                            for stat_row in stat_cursor:
                                zone_area = stat_row[0]
                                ud, weighted_ud = self.calculate_weighted_utilisation_density(builtup_area, stat_row[1])
                                log("Zone Area = %s" % str(zone_area))

                        log("Calculating DIS, TS, UP, WUP")
                        # calling calculation functions:
                        dis, weighted_dis = self.calculate_weighted_dis(si_value_arr)
                        total_sprawl = self.calculate_ts(si_value_arr, raster_cellsize)
                        urban_permeation = self.calculate_up(zone_area, total_sprawl)
                        weighted_urban_proliferation = urban_permeation * weighted_dis * weighted_ud

                        log("Reporting Unit ID = %s, "
                            "DIS = %s, "
                            "wDIS = %s, "
                            "TS = %s, "
                            "UP = %s, "
                            "UD = %s, "
                            "wUD = %s "
                            "WUP = %s"
                            % (str(row[0]), dis, weighted_dis, total_sprawl,
                               urban_permeation, ud, weighted_ud, weighted_urban_proliferation))

                        row[3] = dis
                        row[4] = weighted_dis
                        row[5] = total_sprawl
                        row[6] = urban_permeation
                        row[7] = ud
                        row[8] = weighted_ud
                        row[9] = weighted_urban_proliferation

                        cur.updateRow(row)
                    except Exception as e:
                        arcpy.AddError('Error: ' + str(e))
        finally:
            arcpy.ResetProgressor()


class UrbanSprawlMetricsRasterImpl(MetricsCalcTool):
    # todo: remove params to one input layer
    fields = {}
    _classes_template = {}
    dis_field = 'DIS'
    wdis_field = 'w1DIS'
    ts_field = 'TS'
    up_field = 'UP'
    ud_field = 'UD'
    wud_field = 'w2UD'
    wup_field = 'WUP'

    def __init__(self):
        MetricsCalcTool.__init__(self)
        self.label = "Urban Sprawl Metrics"
        self.description = '''Calculates WUP for each statistical zone'''

    def getParameterInfo(self):
        params = MetricsCalcTool.getParameterInfo(self)
        builtup_areas = arcpy.Parameter(
            displayName="Builtup Raster",
            name="builtup_areas",
            datatype="GPRasterLayer",
            parameterType="Required",
            direction="Input")

        number_of_inhabitants_jobs_field = arcpy.Parameter(
            displayName="Number of Inhabitants and Jobs Field",
            name="number_of_inhabitants_jobs_field",
            datatype="Field",
            parameterType="Required",
            direction="Input")
        number_of_inhabitants_jobs_field.parameterDependencies = [params[1].name]

        horizon_of_perception = arcpy.Parameter(
            displayName="Horizon Of Perception",
            name="horizon_of_perception",
            datatype="GPDouble",
            parameterType="Required",
            direction="Input")
        horizon_of_perception.value = 2000

        checkbox_split_stat_layer = arcpy.Parameter(
            displayName="Split Statistical Layer into multiple Feature Classes (recommended for big datasets)",
            name="checkbox_split_stat_layer",
            datatype="GPBoolean",
            parameterType="Optional",
            direction="Input")

        split_workspace = arcpy.Parameter(
            displayName="Workspace for splitting statistical zones",
            name="split_workspace",
            datatype="Workspace",
            parameterType="Optional",
            direction="Input")

        split_field = arcpy.Parameter(
            displayName="Select field for splitting statistical zones",
            name="split_field",
            datatype="Field",
            parameterType="Optional",
            direction="Input")
        split_field.parameterDependencies = [params[1].name]

        return [builtup_areas, params[1], number_of_inhabitants_jobs_field,
                horizon_of_perception, checkbox_split_stat_layer, split_workspace, split_field]

    def updateParameters(self, params):
        MetricsCalcTool.updateParameters(self, params)
        parameters = ScriptParameters(params)
        checkbox_split_stat_layer = parameters.checkbox_split_stat_layer
        split_workspace = parameters.split_workspace
        split_field = parameters.split_field

        if checkbox_split_stat_layer.value == True:
            split_workspace.enabled = True
            split_field.enabled = True
        else:
            split_workspace.enabled = False
            split_field.enabled = False
        return

    def calculate_si_values(self, dist_arr, wcc):
        """Calculates SI-Values for input array of inter-patch distances like [d1,d2,...dn]"""
        si_value_calc = 0

        for d_ik in dist_arr:
            si_value_calc += (math.sqrt(2 * d_ik + 1) - 1)
        number_urban_cells_within_hp = len(dist_arr)
        si_value = (1 / number_urban_cells_within_hp) * (si_value_calc + wcc)

        return si_value, number_urban_cells_within_hp

    def convert_raster_to_numpy_array_and_get_rasterinfo(self, in_raster):
        """Convert .tif to numpy_array, load it into GDAL Raster to get cell size and """
        inDs = arcpy.RasterToNumPyArray(in_raster)
        # Convert 0 values to nan to speed it up
        inDs = inDs.astype('float')
        inDs[inDs == 0] = 'nan'
        return inDs

    def dist_euclid(self, cell_a, cell_b):
        """Calculates euclidean distance between two distances"""
        a = numpy.asarray(cell_a)
        b = numpy.asarray(cell_b)
        dist = numpy.linalg.norm(a - b)
        return dist

    def cmask(self, coords, radius, array):
        """Masking is important to speed up processes"""
        a, b = coords[0], coords[1]
        nx, ny = array.shape
        y, x = numpy.ogrid[-a:nx - a, -b:ny - b]
        # create mask where each value in radius around coords is true and everything else is false
        mask = x * x + y * y <= radius * radius

        # reshape mask to rectangular shape
        i, j = numpy.where(mask)
        indices = numpy.meshgrid(numpy.arange(min(i), max(i) + 1),
                                 numpy.arange(min(j), max(j) + 1),
                                 indexing='ij')
        masked_array = array[tuple(indices)]

        # get indices the source_coordinates have in masked index
        coords_of_source_cell_in_mask = list(numpy.argwhere((indices[0] == coords[0]) &
                                                            (indices[1] == coords[1]))[0])

        return masked_array, coords_of_source_cell_in_mask

    def calculate_si_values_for_numpy_array(self, in_array, horizon_of_perception, cell_size, wcc):
        """For each cell make slice sub-array around the cell and calculate distances to every other cell (value=1)"""
        start_time = time.time()
        si_array = in_array.astype(float)
        count_urban_pixels = numpy.count_nonzero(si_array == 1)
        arcpy.SetProgressor("step", "Processing Features...",
                            0, count_urban_pixels, 1)
        progressor = 0
        for (source_x, source_y), source_value in numpy.ndenumerate(in_array):
            progress_percentage = round((progressor / count_urban_pixels) * 100, 2)
            arcpy.SetProgressorLabel("Processed {0} of {1} Features ... {2} %"
                                     .format(progressor,
                                             count_urban_pixels,
                                             progress_percentage))

            if source_value == 1:
                arcpy.SetProgressorPosition()
                progressor += 1
                # log("... pixel {0}. coords {1} / {2}".format(progressor, source_x, source_y))
                """Mask radius must be HP / cellsize (because thats the unit in the numpy array)"""
                mask_radius = horizon_of_perception / cell_size
                masked_array, source_coords_in_masked_array = self.cmask([source_x, source_y], mask_radius, in_array)

                dist_arr = []

                for (target_x, target_y), target_value in numpy.ndenumerate(masked_array):
                    if target_value == 1 and (source_coords_in_masked_array != [target_x, target_y]):

                        dist = self.dist_euclid(source_coords_in_masked_array,
                                                [target_x, target_y]) * cell_size
                        if dist <= horizon_of_perception:
                            dist_arr.append(dist)
                    else:
                        pass

                weighted_distances = []
                for d_ik in dist_arr:
                    si_value_calc = ((math.sqrt(2 * d_ik + 1)) - 1)
                    weighted_distances.append(si_value_calc)

                si_value = (sum(weighted_distances) + wcc) / (len(dist_arr) + 1)

                si_array[source_x, source_y] = si_value
            else:
                pass

        log("--- Si-value computation took %s seconds ---" % (time.time() - start_time))
        return si_array

    def si_computation(self, builtup_areas, stat_layer, horizon_of_perception, raster_cellsize, wcc):
        """Clip Raster by Stat Layer in order to just analyze urban areas within the reporting units"""

        # get extent of the stat layer (needed for clippring)
        desc_stat_layer = arcpy.Describe(stat_layer)
        x_min, y_min, x_max, y_max = desc_stat_layer.extent.XMin - horizon_of_perception, \
                                     desc_stat_layer.extent.YMin - horizon_of_perception, \
                                     desc_stat_layer.extent.XMax + horizon_of_perception, \
                                     desc_stat_layer.extent.YMax + horizon_of_perception

        clipping_rectangle = "{} {} {} {}".format(x_min, y_min, x_max, y_max)
        log("Extent of raster that the SI-values are calculated for :" + clipping_rectangle)
        builtup_raster_clipped = self.create_temp_layer('builtup_raster_clipped')
        arcpy.Clip_management(builtup_areas, clipping_rectangle, builtup_raster_clipped, "#", "#", "NONE",
                              "NO_MAINTAIN_EXTENT")

        arcpy.env.outputCoordinateSystem = builtup_raster_clipped  # Declare CRS
        inDs_numpy = self.convert_raster_to_numpy_array_and_get_rasterinfo(builtup_raster_clipped)

        """Calculate Si-Values"""
        si_array = self.calculate_si_values_for_numpy_array(inDs_numpy, horizon_of_perception,
                                                            raster_cellsize, wcc)
        """Convert Si-Array into Point Feature-Class"""
        builtup_raster = arcpy.Raster(builtup_raster_clipped)
        lowerLeft = arcpy.Point(builtup_raster.extent.XMin, builtup_raster.extent.YMin)
        si_arr_to_raster = arcpy.NumPyArrayToRaster(si_array, lowerLeft, raster_cellsize)
        """Convert Si-Raster to Points"""
        builtup_points = self.create_temp_layer('builtup_points')
        arcpy.RasterToPoint_conversion(si_arr_to_raster, builtup_points)

        builtup_points_copy = self.create_temp_layer('builtup_points_selected')
        arcpy.MakeFeatureLayer_management(builtup_points, builtup_points_copy)
        """Select only urban points:"""
        relevant_points_clause = ' "grid_code" <> 127 AND "grid_code" <> 0'
        arcpy.SelectLayerByAttribute_management(builtup_points_copy, "NEW_SELECTION", relevant_points_clause)
        builtup_points_selected = self.create_temp_layer('buf_nBuf_join_selected')
        arcpy.CopyFeatures_management(builtup_points_copy, builtup_points_selected)
        """Add si-value field and copy values from grid-code field"""
        arcpy.AddField_management(builtup_points_selected, "si_value", "DOUBLE")
        with arcpy.da.UpdateCursor(builtup_points_selected, ["si_value", "grid_code"]) as cursor:
            for row in cursor:
                row[0] = row[1]
                cursor.updateRow(row)
        del cursor

        return builtup_points_selected

    def calculate_weighted_dis(self, si_array):
        """ Calculating dispersion"""
        # delete all zero values for better
        n = len(si_array)

        dis = (1 / n) * sum(si_array)
        # weighted DIS (Jaeger & Schwick 2014:299)
        wdis = 0.5 + (math.exp(0.294432 * dis - 12.955) / (1 + math.exp(0.294432 * dis - 12.955)))
        return dis, wdis

    def calculate_ts(self, si_array, cell_size):
        """ Calculating total sprawl"""
        ts = math.pow(cell_size, 2) * sum(si_array)
        return ts

    def calculate_up(self, total_zone_area, ts):
        """ Calculating urban permeation"""
        up = 1 / total_zone_area * ts
        return up

    def calculate_weighted_utilisation_density(self, builtup_area, number_inhabitants_jobs):
        """ Calculating weighted UD"""
        # todo: find out if ud is in ha or km2
        builtup_area_km2 = builtup_area / 1000000
        ud = number_inhabitants_jobs / builtup_area_km2
        weighted_utilisation_density = math.exp(4.159 - 0.000613125 * ud) / (1 + math.exp(4.159 - 0.000613125 * ud))
        return ud, weighted_utilisation_density

    def execute(self, parameters, messages):
        setup_debug_mode()
        arcpy.SetLogHistory(True)
        # getting params
        input_parameters = ScriptParameters(parameters)
        builtup_areas = input_parameters.builtup_areas.valueAsText
        stat_layer = input_parameters.stat_layer.valueAsText
        number_of_inhabitants_jobs_field = input_parameters.number_of_inhabitants_jobs_field.valueAsText
        horizon_of_perception = input_parameters.horizon_of_perception.value
        split_workspace = input_parameters.split_workspace.valueAsText
        split_field = input_parameters.split_field.valueAsText
        checkbox_split_stat_layer = input_parameters.split_field.valueAsText

        stat_layer_list = []
        if checkbox_split_stat_layer:
            '''If stat layer should be split into multiple FC:'''
            ###
            log("Stat Layer gets split into multiple Classes by field: %s" % split_field)
            '''Get all values of split field in order to get the names of the created FCs'''

            arcpy.SetProgressorLabel("Splitting Statistical Layer...")
            with arcpy.da.SearchCursor(stat_layer, [split_field]) as split_field_cur:
                for split_row in split_field_cur:
                    if isinstance(split_row[0], str):
                        layer_name = split_workspace + "\\T" + split_row[0]
                    else:
                        layer_name = split_workspace + "\\T" + str(split_row[0])
                    log("Part-Stat-Zone = %s" % layer_name)
                    stat_layer_list.append(layer_name)

            arcpy.SplitByAttributes_analysis(stat_layer, split_workspace, split_field)

        else:
            stat_layer_list = [stat_layer]

        '''Iterate over all stat_layers (split or not) and run tool code'''
        for current_stat_layer in stat_layer_list:
            log("---")
            log("Executing tool for stat zone: %s" % current_stat_layer)
            log("---")

            current_stat_layer_name = current_stat_layer
            '''Get Raster Properties (cellsize to declare wcc)'''
            raster_properties = arcpy.GetRasterProperties_management(builtup_areas, "CELLSIZEX")
            raster_cellsize = int(raster_properties.getOutput(0))
            log("Raster Cellsize = %s" % raster_cellsize)
            wcc = get_wcc(raster_cellsize)
            log("wcc = %s" % wcc)

            # stat_layer = parameters.stat_layer.valueAsText

            MetricsCalcTool.prepare_stat_layer(current_stat_layer_name,
                                               EnsureUnitIDFieldExists(),
                                               EnsureFieldExists(self.dis_field, "DOUBLE", 0),
                                               EnsureFieldExists(self.wdis_field, "DOUBLE", 0),
                                               EnsureFieldExists(self.ts_field, "DOUBLE", 0),
                                               EnsureFieldExists(self.up_field, "DOUBLE", 0),
                                               EnsureFieldExists(self.ud_field, "DOUBLE", 0),
                                               EnsureFieldExists(self.wud_field, "DOUBLE", 0),
                                               EnsureFieldExists(self.wup_field, "DOUBLE", 0),
                                               )

            msg = "Calculating SI_Values..."
            log(msg)

            '''Caclulating si_values...'''
            builtup_points_selected = self.si_computation(builtup_areas, current_stat_layer_name,
                                                          horizon_of_perception, raster_cellsize,
                                                          wcc)

            '''Calculate WUP metrics for each statistical zone'''
            try:
                stat_layer_fields = [UNIT_ID_FIELD_NAME,
                                     "SHAPE@AREA",
                                     number_of_inhabitants_jobs_field,
                                     self.dis_field,  # row[3] in cur
                                     self.wdis_field,  # row[4]
                                     self.ts_field,  # row[5]
                                     self.up_field,  # row[6]
                                     self.ud_field,  # row[7]
                                     self.wud_field,  # row[8]
                                     self.wup_field  # row[9]
                                     ]

                with intersect_analyzed_with_stat_layer(current_stat_layer, builtup_points_selected) as Intersection, \
                        UpdateCursor(current_stat_layer_name, stat_layer_fields) as cur:
                    for row in cur:
                        si_value_arr = []
                        builtup_area = 0
                        where = "%s=%s" % (UNIT_ID_FIELD_NAME, row[0])
                        arcpy.SetProgressorPosition()
                        arcpy.SetProgressorLabel("Calculating Metrics")
                        try:
                            with arcpy.da.SearchCursor(Intersection,
                                                       ("SHAPE@AREA", "si_value"),
                                                       where) \
                                    as intersectCur:
                                for intersectCurRow in intersectCur:
                                    # For each point add cellsize² to builtup area (the builtup area is needed for UD calculation)
                                    builtup_area += math.pow(raster_cellsize, 2)
                                    si_value_arr.append(intersectCurRow[1])

                            log("Urban Area = %s" % builtup_area)

                            zone_area = 0
                            weighted_ud = 0
                            zone_where_clause = "%s=%s" % (UNIT_ID_FIELD_NAME, row[0])
                            with arcpy.da.SearchCursor(current_stat_layer_name,
                                                       ["SHAPE@AREA", number_of_inhabitants_jobs_field],
                                                       zone_where_clause) as stat_cursor:
                                for stat_row in stat_cursor:
                                    zone_area = stat_row[0]
                                    ud, weighted_ud = self.calculate_weighted_utilisation_density(builtup_area,
                                                                                                  stat_row[1])
                                    log("Zone Area = %s" % str(zone_area))

                            log("Calculating DIS, TS, UP, WUP")
                            # calling calculation functions:
                            dis, weighted_dis = self.calculate_weighted_dis(si_value_arr)
                            total_sprawl = self.calculate_ts(si_value_arr, raster_cellsize)
                            urban_permeation = self.calculate_up(zone_area, total_sprawl)
                            weighted_urban_proliferation = urban_permeation * weighted_dis * weighted_ud

                            log("Reporting Unit ID = %s, "
                                "DIS = %s, "
                                "wDIS = %s, "
                                "TS = %s, "
                                "UP = %s, "
                                "UD = %s, "
                                "wUD = %s "
                                "WUP = %s"
                                % (str(row[0]), dis, weighted_dis, total_sprawl,
                                   urban_permeation, ud, weighted_ud, weighted_urban_proliferation))

                            row[3] = dis
                            row[4] = weighted_dis
                            row[5] = total_sprawl
                            row[6] = urban_permeation
                            row[7] = ud
                            row[8] = weighted_ud
                            row[9] = weighted_urban_proliferation

                            cur.updateRow(row)
                        except Exception as e:
                            arcpy.AddError('Error: ' + str(e))
            finally:
                arcpy.ResetProgressor()

        '''If stat layer was split, merge split layer to single one'''
        if len(stat_layer_list) > 1:
            merged_stat_layer = stat_layer + "_merged"
            arcpy.Merge_management(stat_layer_list, merged_stat_layer)
            '''Delete all split layers'''
            for layer in stat_layer_list:
                delete_if_exists(layer)


class ContrastMetricsTool(MetricsCalcTool):
    def __init__(self):
        MetricsCalcTool.__init__(self)
        self.label = "Contrast Metrics"
        self.description = '''for analyzed class calculates edge length of shared boundaries with selected contrast classes.'''

    def getParameterInfo(self):
        params = MetricsCalcTool.getParameterInfo(self)
        parameters = ScriptParameters(params)
        parameters.class_list.parameterType = "Required"
        parameters.class_list.displayName = "Contrast classes"

        analyzedClass = arcpy.Parameter(
            displayName="Analyzed class",
            name="analyzed_class",
            datatype="GPString",
            parameterType="Required",
            direction="Input",
        )
        analyzedClass.parameterDependencies = [parameters.in_area.name]
        analyzedClass.filter.type = parameters.class_list.filter.type
        analyzedClass.filter.list = list(parameters.class_list.filter.list)

        params.insert(params.index(parameters.class_list), analyzedClass)
        return params

    def updateParameters(self, params):
        MetricsCalcTool.updateParameters(self, params)
        parameters = ScriptParameters(params)
        analyzedClass = parameters.analyzed_class
        if parameters.class_field.altered:
            analyzedClass.filter.list = list(parameters.class_list.filter.list)

    def execute(self, parameters, messages):
        setup_debug_mode()
        input_parameters = ScriptParameters(parameters)
        input_area = input_parameters.in_area.valueAsText
        stats_out = input_parameters.stat_layer.valueAsText
        classFieldName = input_parameters.class_field.valueAsText
        classField = get_field(input_area, classFieldName)
        analyzed_class = getParameterValues(input_parameters.analyzed_class)

        classList = getParameterValues(input_parameters.class_list)
        if not classList or len(classList) == 0:
            classList = self.get_all_classes(input_area, classFieldName)
        classList = list(map(str, classList))

        self.prepare_stat_layer(stats_out,
                                EnsureUnitIDFieldExists(), )

        analyzedClassEdgeLength = "el_a_class"
        fields = [analyzedClassEdgeLength]
        fieldNames = {}
        for cl in classList:
            sharedEdgeLengthFieldName = arcpy.ValidateFieldName('el%s' % (cl))[:10]
            contrastIndexFieldName = arcpy.ValidateFieldName('cce%s' % (cl))[:10]
            fields += [sharedEdgeLengthFieldName, contrastIndexFieldName]
            fieldNames[cl] = {'edgeLength': sharedEdgeLengthFieldName,
                              'contrastIndex': contrastIndexFieldName}
        try:
            for fieldName in fields:
                log('Adding field %s to %s' % (fieldName, stats_out))
                arcpy.AddField_management(stats_out, fieldName, "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED",
                                          "")

            msg = "Counting Class Edge Contrast Index"
            log(msg)
            arcpy.SetProgressor("step", msg, 0, int(arcpy.GetCount_management(stats_out).getOutput(0)) * 2, 1)

            with createTempLayer('analyzed_class') as analyzedInputClass, \
                    createTempLayer('analyzedInputClassEdges') as analyzedInputClassEdges, \
                    createTempLayer('analyzedInputClassBuffered') as analyzedInputClassBuffered, \
                    createTempLayer('boundaryInputClasses') as boundaryInputClasses, \
                    createTempLayer('boundaryInputClassesEdges') as boundaryInputClassesEdges, \
                    createTempLayer('boundaryInputClassesEdgesClipped') as boundaryInputClassesEdgesClipped:

                select_features_from_feature_class(classField, [analyzed_class], input_area, analyzedInputClass)
                arcpy.FeatureToLine_management(analyzedInputClass, analyzedInputClassEdges)
                arcpy.Buffer_analysis(analyzedInputClassEdges, analyzedInputClassBuffered,
                                      buffer_distance_or_field='0.05 meter', line_end_type='FLAT')

                select_features_from_feature_class(classField, classList, input_area, boundaryInputClasses)
                arcpy.Snap_edit(analyzedInputClass, [[boundaryInputClasses, 'EDGE', '1 meter']])
                arcpy.FeatureToLine_management(boundaryInputClasses, boundaryInputClassesEdges)
                arcpy.Clip_analysis(in_features=boundaryInputClassesEdges, clip_features=analyzedInputClassBuffered,
                                    out_feature_class=boundaryInputClassesEdgesClipped)

                # calculate total edge length
                # TODO - integrate with Edge Density tool
                with intersect_analyzed_with_stat_layer(stats_out,
                                                        analyzedInputClassEdges) as edgesIntersection, UpdateCursor(
                    stats_out, (UNIT_ID_FIELD_NAME, analyzedClassEdgeLength)) as cur:
                    for row in cur:
                        arcpy.SetProgressorPosition()
                        try:
                            where = "%s=%s" % (UNIT_ID_FIELD_NAME, row[0])
                            with SearchCursor(edgesIntersection, ("SHAPE@LENGTH",), where) as cur2:
                                edgesLength = sum(row[0] for row in cur2)
                                row[1] = edgesLength
                            cur.updateRow(row)
                        except Exception as e:
                            handleException(e)

                with intersect_analyzed_with_stat_layer(stats_out,
                                                        boundaryInputClassesEdgesClipped) as edgesIntersection, UpdateCursor(
                    stats_out, [UNIT_ID_FIELD_NAME, 'SHAPE@'] + fields) as cur:
                    cursorFields = cur.fields
                    analyzedClassEdgeLengthPos = cursorFields.index(analyzedClassEdgeLength)
                    for row in cur:
                        arcpy.SetProgressorPosition()
                        try:
                            where = "%s=%s" % (UNIT_ID_FIELD_NAME, row[0])
                            with SearchCursor(edgesIntersection, ("SHAPE@LENGTH", classFieldName), where) as cur2:
                                cnt = Counter()
                                for row2 in cur2:
                                    className = str(row2[1])
                                    edgeLength = row2[0]
                                    cnt[className] += edgeLength
                                for className, edgeLength in list(cnt.items()):
                                    fieldName = fieldNames[className]
                                    lengthPos = cursorFields.index(fieldName['edgeLength'])
                                    contrastIndexPos = cursorFields.index(fieldName['contrastIndex'])
                                    row[lengthPos] = edgeLength
                                    row[contrastIndexPos] = edgeLength / row[analyzedClassEdgeLengthPos] * 100
                            cur.updateRow(row)
                        except Exception as e:
                            handleException(e)
            arcpy.ResetProgressor()
        except Exception as e:
            arcpy.DeleteField_management(stats_out, fields)
            handleException(e)


class ConnectanceMetricsTool(MetricsCalcTool):
    PATCH_AREA_PERCENT_FIELD_NAME = 'ci_pp'  # percentage of patch area
    PATCH_AREA_FIELD_NAME = 'ci_pa'  # patch area in range of connection
    CONNECTION_AREA_PERCENTAGE_FIELD_NAME = 'ci_cp'  # percentage of connection area to hex area
    CONNECTION_AREA_FIELD_NAME = 'ci_ca'  # connection area
    NUMBER_OF_PATCHES_FIELD_NAME = 'ci_np'  # number of connected patches (by distinct id)

    def __init__(self):
        MetricsCalcTool.__init__(self)

        self.label = "Connectance Metrics"
        self.description = '''Calculates a Connectance Index and Connection area layer within a given distance between selected classes.
        The detailed explanation can be found here https://docs.google.com/drawings/d/1dQ2WId9RD5sfMeJvY63-uVbrSNz228fHe3JDkYe3-6o/edit?usp=sharing<br/>
        Outputs:
<ul>
   <li> <em>ci_np</em> - number of distinct connected patches (by FID) </li>
   <li> <em>ci_pa</em> - patch area within range of connection </li>
   <li> <em>ci_pp</em> - percentage of patch area within range of connection to statistical zone area</li>
   <li> <em>ci_ca</em> - area of connection zone between patches</li>
   <li> <em>ci_cp</em> - percentage of connection zone between patches to statistical zone area</li>
</ul>'''

    def getParameterInfo(self):
        params = MetricsCalcTool.getParameterInfo(self)
        parameters = ScriptParameters(params)
        parameters.class_list.parameterType = "Required"
        parameters.class_list.displayName = "Classes (will be merged)"

        max_distance = arcpy.Parameter(
            displayName="Maximum connection distance (map units)",
            name="conn_distance",
            datatype="GPDouble",
            parameterType="Required",
            direction="Input",
        )
        params.insert(2, max_distance)

        max_allowable_distance_param = arcpy.Parameter(
            displayName="Maximum Allowable Offset (feature units)",
            name="max_allowable_distance",
            datatype="GPLong",
            parameterType="Required",
            direction="Input",
        )
        max_allowable_distance_param.value = 150
        params.append(max_allowable_distance_param)

        out_connections_param = arcpy.Parameter(
            displayName="Output connections layer",
            name="out_connections",
            datatype="DEShapefile",
            parameterType="Optional",
            direction="Output",
        )
        params.append(out_connections_param)

        return params

    def updateParameters(self, params):
        MetricsCalcTool.updateParameters(self, params)

    def _copy_fid_to_patch_id(self, selected_features):
        """ Copy FID of selected features to separate column """
        OID_Field = arcpy.Describe(input_area).OIDFieldName
        arcpy.AddField_management(selected_features, 'patchID', 'LONG')
        with UpdateCursor(selected_features, [OID_Field, 'patchID']) as selectedFeaturesCursor:
            for row in selectedFeaturesCursor:
                row[1] = row[0]
                selectedFeaturesCursor.updateRow(row)

    def ___prepare_input_layers(self, input_area, stat_layer, class_field, class_list):
        stat_layer_dissolved = self.create_temp_layer('stat_layer_dissolved')
        arcpy.Dissolve_management(stat_layer, stat_layer_dissolved)
        selected_features = self.create_temp_layer('selected_features')
        select_features_from_feature_class(class_field, class_list, input_area, selected_features)
        union_features = self.create_temp_layer('union_features')
        arcpy.Union_analysis([selected_features], union_features, join_attributes='ONLY_FID')
        input_layer_dissolved = self.create_temp_layer('input_layer_dissolved')
        arcpy.Dissolve_management(union_features, input_layer_dissolved, multi_part=False)
        return input_layer_dissolved, selected_features, stat_layer_dissolved

    def _generate_connections(self, params):
        prepared_input_layers = self.___prepare_input_layers(params.input_area, params.stat_layer, params.class_field,
                                                             params.class_list)
        input_layer_dissolved, selected_features, stat_layer_dissolved = prepared_input_layers
        log('Generating connections')

        input_simplified = self._simplify_input(input_layer_dissolved, params.conn_distance,
                                                params.max_allowable_distance)

        patches_near = self.create_temp_layer('patches_near')
        arcpy.GenerateNearTable_analysis(in_features=input_simplified,
                                         near_features=input_simplified,
                                         out_table=patches_near,
                                         search_radius=params.conn_distance,
                                         closest=False)

        connections_polygons = self.create_temp_layer('connections_polygons')
        arcpy.CreateFeatureclass_management(out_path=os.path.dirname(connections_polygons),
                                            out_name=os.path.splitext(os.path.basename(connections_polygons))[0],
                                            geometry_type='POLYGON',
                                            spatial_reference=arcpy.Describe(input_simplified).spatialReference)
        arcpy.AddField_management(in_table=connections_polygons, field_name='FID1', field_type='LONG')
        arcpy.AddField_management(in_table=connections_polygons, field_name='FID2', field_type='LONG')

        cnt = arcpy.GetCount_management(patches_near)[0]
        connections_cur = arcpy.da.InsertCursor(connections_polygons, ['SHAPE@', 'FID1', 'FID2'])
        current = 0

        done = set()
        arcpy.SetProgressor('step', 'Creating connections', 0, int(cnt), 1)

        for in_fid, near_fid in arcpy.da.SearchCursor(patches_near, ['IN_FID', 'NEAR_FID']):
            current += 1
            arcpy.SetProgressorLabel('Creating connections %s/%s' % (current, cnt))
            arcpy.SetProgressorPosition(current)

            fids = (in_fid, near_fid)
            if fids in done or tuple(reversed(fids)) in done:
                continue
            log('Creating connection polygon for FIDs: %s, %s' % (in_fid, near_fid))

            done.add(fids)
            where_clause = 'FID=%d OR FID=%d' % fids
            g1, g2 = [row[0] for row in arcpy.da.SearchCursor(input_simplified, ['SHAPE@'], where_clause=where_clause)]

            g1_buffer = g1.buffer(params.conn_distance)
            g2_buffer = g2.buffer(params.conn_distance)

            g1_int = g1.intersect(g2_buffer, 4)
            g2_int = g2.intersect(g1_buffer, 4)

            del g1_buffer, g2_buffer

            lines = []
            for g in [g1, g2, g1_int, g2_int]:
                g_as_line = arcpy.PolygonToLine_management(g, arcpy.Geometry(), neighbor_option='IGNORE_NEIGHBORS')
                if g_as_line:
                    lines.append(g_as_line[0])
            if len(lines) != 4:
                continue
            g1_line, g2_line, g1_line_int, g2_line_int = tuple(lines)

            g1_clipped_to_buffer = g1_line.intersect(g1_line_int, 2)
            g2_clipped_to_buffer = g2_line.intersect(g2_line_int, 2)

            lines1 = arcpy.SplitLine_management(g1_clipped_to_buffer, arcpy.Geometry())
            lines2 = arcpy.SplitLine_management(g2_clipped_to_buffer, arcpy.Geometry())

            polygons = []
            # print len(lines1), len(lines2), g1_clipped_to_buffer.length, g2_clipped_to_buffer.length
            for line1, line2 in itertools.product(lines1, lines2):
                if line1 == line2:
                    continue
                line1_p1 = line1.firstPoint
                line1_p2 = line1.lastPoint
                line2_p1 = line2.firstPoint
                line2_p2 = line2.lastPoint

                distances = [
                    line1.distanceTo(line2_p1) <= params.conn_distance,
                    line1.distanceTo(line2_p2) <= params.conn_distance,

                    line2.distanceTo(line1_p1) <= params.conn_distance,
                    line2.distanceTo(line1_p2) <= params.conn_distance,
                ]

                if sum(distances) == 4:
                    lines_union = line1.union(line2)
                    polygons.append(lines_union.convexHull())
            if polygons:
                connection = arcpy.Dissolve_management(polygons, arcpy.Geometry())[0]
                # in rare cases dissolve returns polyline
                if connection.type == 'polygon':
                    connections_cur.insertRow([connection, in_fid, near_fid])
                del connection
            del polygons, g1_line, g2_line, g1_line_int, g2_line_int, g1_clipped_to_buffer, g2_clipped_to_buffer

        connections_polygons_erased = self.create_temp_layer('connections_polygons_erased')
        arcpy.Erase_analysis(in_features=connections_polygons, erase_features=input_layer_dissolved,
                             out_feature_class=connections_polygons_erased)

        lines_buffers_cleaned = self._create_connections_buffers(connections_polygons_erased,
                                                                 input_layer_dissolved, input_simplified,
                                                                 params)

        arcpy.Append_management(inputs=lines_buffers_cleaned,
                                target=connections_polygons_erased,
                                schema_type='NO_TEST')

        connections_polygons_erased_dissolved = self.create_temp_layer('connections_polygons_erased_dissolved')
        arcpy.Dissolve_management(in_features=connections_polygons_erased,
                                  out_feature_class=connections_polygons_erased_dissolved)

        connections_polygons_erased_2 = self.create_temp_layer('connections_polygons_erased_2')
        arcpy.Erase_analysis(in_features=connections_polygons_erased_dissolved, erase_features=input_layer_dissolved,
                             out_feature_class=connections_polygons_erased_2)

        singlepart_connections = self.create_temp_layer('singlepart_connections')
        arcpy.MultipartToSinglepart_management(in_features=connections_polygons_erased_2,
                                               out_feature_class=singlepart_connections)

        near_table = self.create_temp_layer('near_table')
        arcpy.GenerateNearTable_analysis(in_features=singlepart_connections,
                                         near_features=input_layer_dissolved,
                                         out_table=near_table,
                                         search_radius=0,
                                         closest=False)
        near_table_count = self.create_temp_layer('near_table_count')
        arcpy.Frequency_analysis(in_table=near_table,
                                 out_table=near_table_count,
                                 frequency_fields='IN_FID')
        to_remove = set()
        with arcpy.da.SearchCursor(near_table_count,
                                   field_names=['IN_FID'],
                                   where_clause='FREQUENCY=1') as count_cursor:
            for row in count_cursor:
                to_remove.add(row[0])
        log_debug('Loose connections to remove: %s' % to_remove)
        connections_cleaned = self.create_temp_layer('connections_cleaned')
        arcpy.CreateFeatureclass_management(out_path=os.path.dirname(connections_cleaned),
                                            out_name=os.path.splitext(os.path.basename(connections_cleaned))[0],
                                            geometry_type='POLYGON',
                                            spatial_reference=arcpy.Describe(input_simplified).spatialReference)
        connections_cleaned_cur = arcpy.da.InsertCursor(connections_cleaned, ['OID@', 'SHAPE@'])
        search_cursor = arcpy.da.SearchCursor(singlepart_connections,
                                              field_names=['OID@', 'SHAPE@'])
        for conn_row in search_cursor:
            fid = conn_row[0]
            if fid not in to_remove:
                connections_cleaned_cur.insertRow(conn_row)

        connection_final_dissolved = self.create_temp_layer('connection_final_dissolved')
        arcpy.Dissolve_management(connections_cleaned, connection_final_dissolved)

        connections = self.create_temp_layer('connections')
        arcpy.Clip_analysis(in_features=connection_final_dissolved,
                            clip_features=stat_layer_dissolved,
                            out_feature_class=connections)

        log('Created connections')
        if params.out_connections:
            arcpy.CopyFeatures_management(connections, params.out_connections)

        return connections

    def _create_connections_buffers(self, connections_polygons_erased, input_layer_dissolved, input_simplified, params):
        connections_polygons_lines = self.create_temp_layer('connections_polygons_lines')
        arcpy.PolygonToLine_management(in_features=connections_polygons_erased,
                                       out_feature_class=connections_polygons_lines, neighbor_option=False)
        simplified_lines = self.create_temp_layer('simplified_lines')
        arcpy.PolygonToLine_management(in_features=input_simplified,
                                       out_feature_class=simplified_lines, neighbor_option=False)
        connections_border_lines_multi = self.create_temp_layer('connections_border_lines_multi')
        arcpy.Intersect_analysis(in_features=[connections_polygons_lines, simplified_lines],
                                 out_feature_class=connections_border_lines_multi)
        connections_border_lines = self.create_temp_layer('connections_border_lines')
        arcpy.MultipartToSinglepart_management(in_features=connections_border_lines_multi,
                                               out_feature_class=connections_border_lines)
        connections_border_lines_buffer_1 = self.create_temp_layer('connections_border_lines_buffer_1')
        connections_border_lines_buffer_2 = self.create_temp_layer('connections_border_lines_buffer_2')
        connections_border_lines_buffer = self.create_temp_layer('connections_border_lines_buffer')
        arcpy.Buffer_analysis(in_features=connections_border_lines,
                              out_feature_class=connections_border_lines_buffer_1,
                              buffer_distance_or_field=params.max_allowable_distance,
                              line_end_type='FLAT',
                              line_side='LEFT')
        arcpy.Buffer_analysis(in_features=connections_border_lines,
                              out_feature_class=connections_border_lines_buffer_2,
                              buffer_distance_or_field=params.max_allowable_distance,
                              line_end_type='FLAT',
                              line_side='RIGHT')
        arcpy.Append_management(inputs=connections_border_lines_buffer_1,
                                target=connections_border_lines_buffer_2,
                                schema_type='NO_TEST')
        arcpy.Sort_management(in_dataset=connections_border_lines_buffer_2,
                              out_dataset=connections_border_lines_buffer,
                              sort_field=[['ORIG_FID', 'ASCENDING']])
        arcpy.AddField_management(in_table=connections_border_lines_buffer, field_name='BUFF_ID', field_type='LONG')
        cursor = arcpy.da.UpdateCursor(connections_border_lines_buffer, field_names=['OID@', 'BUFF_ID'])
        for row in cursor:
            row[1] = row[0]
            cursor.updateRow(row)
        buffer_input_tabulate_intersect = self.create_temp_layer('buffer_tabulate_intersect')
        arcpy.TabulateIntersection_analysis(
            in_zone_features=connections_border_lines_buffer,
            zone_fields=['BUFF_ID'],
            in_class_features=input_layer_dissolved,
            out_table=buffer_input_tabulate_intersect
        )
        arcpy.JoinField_management(in_data=connections_border_lines_buffer,
                                   in_field='BUFF_ID',
                                   join_table=buffer_input_tabulate_intersect,
                                   join_field='BUFF_ID')

        lines_buffers_cleaned = self.create_temp_layer('lines_buffers_cleaned')
        arcpy.CreateFeatureclass_management(out_path=os.path.dirname(lines_buffers_cleaned),
                                            out_name=os.path.splitext(os.path.basename(lines_buffers_cleaned))[0],
                                            geometry_type='POLYGON',
                                            spatial_reference=arcpy.Describe(input_simplified).spatialReference)

        search_cursor = arcpy.da.SearchCursor(connections_border_lines_buffer,
                                              field_names=['ORIG_FID', 'PERCENTAGE', 'SHAPE@'])
        with arcpy.da.InsertCursor(lines_buffers_cleaned, field_names=['SHAPE@']) as insert_cursor:
            for key, rrs in itertools.groupby(search_cursor, key=lambda r: r[0]):
                overlapping_row = max(rrs, key=lambda r: r[1])
                insert_cursor.insertRow([overlapping_row[2]])

        return lines_buffers_cleaned

    class ConnectanceMetricsParameters(object):

        def __init__(self, tool, parameters):
            super(ConnectanceMetricsTool.ConnectanceMetricsParameters, self).__init__()

            parameters_map = dict([(param.name, param) for param in parameters])

            self.input_area = parameters_map['in_area'].valueAsText
            self.stat_layer = parameters_map['stat_layer'].valueAsText
            self.class_field_name = parameters_map['class_field'].valueAsText
            self.class_field = get_field(self.input_area, self.class_field_name)
            self.conn_distance = parameters_map['conn_distance'].value
            self.out_connections = parameters_map['out_connections'].valueAsText
            self.max_allowable_distance = int(parameters_map['max_allowable_distance'].value)

            class_list = getParameterValues(parameters_map['class_list'])
            if not class_list or len(class_list) == 0:
                class_list = tool.get_all_classes(self.input_area, self.class_field_name)
            self.class_list = list(map(str, class_list))

            selected_features = tool.create_temp_layer('selected_features')
            select_features_from_feature_class(self.class_field, class_list, self.input_area, selected_features)
            self.selected_features = selected_features

    def execute(self, parameters, messages):
        setup_debug_mode()

        params = self.ConnectanceMetricsParameters(self, parameters)

        self.prepare_stat_layer(params.stat_layer,
                                EnsureUnitIDFieldExists(),
                                EnsureZoneAreaFieldExists(),
                                EnsureFieldExists(field_name=self.NUMBER_OF_PATCHES_FIELD_NAME, field_type='LONG',
                                                  default_value=0),
                                EnsureFieldExists(field_name=self.CONNECTION_AREA_FIELD_NAME, field_type='DOUBLE',
                                                  default_value=0),
                                EnsureFieldExists(field_name=self.CONNECTION_AREA_PERCENTAGE_FIELD_NAME,
                                                  field_type='DOUBLE', default_value=0),
                                EnsureFieldExists(field_name=self.PATCH_AREA_FIELD_NAME, field_type='DOUBLE',
                                                  default_value=0),
                                EnsureFieldExists(field_name=self.PATCH_AREA_PERCENT_FIELD_NAME, field_type='DOUBLE',
                                                  default_value=0), )

        try:
            msg = "Analyzing"
            log(msg)
            arcpy.SetProgressor("step", msg, 0, int(arcpy.GetCount_management(params.stat_layer).getOutput(0)) * 2, 1)

            layer_to_analyse = self._prepare_layer_to_analyse(params)
            connections = self._generate_connections(params)

            self._analyse_layer(params, layer_to_analyse, connections)

        except Exception as e:
            handleException(e)
        finally:
            arcpy.ResetProgressor()
            self.on_exit()

    def _prepare_layer_to_analyse(self, params):
        input_layer_dissolved = self.create_temp_layer('input_layer_dissolved')
        arcpy.Dissolve_management(in_features=params.selected_features,
                                  out_feature_class=input_layer_dissolved,
                                  multi_part='SINGLE_PART')

        buffer_around_input = self.create_temp_layer('buffer_around_input')
        arcpy.Buffer_analysis(in_features=input_layer_dissolved,
                              out_feature_class=buffer_around_input,
                              buffer_distance_or_field=params.conn_distance,
                              line_side='OUTSIDE_ONLY',
                              line_end_type='ROUND',
                              dissolve_option='NONE')

        layer_to_analyse = self.create_temp_layer('layer_to_analyse')
        arcpy.Intersect_analysis(in_features=[buffer_around_input, params.selected_features],
                                 out_feature_class=layer_to_analyse,
                                 join_attributes='NO_FID')

        self._copy_fid_to_patch_id(layer_to_analyse)
        return layer_to_analyse

    @staticmethod
    def _get_patches_number_and_area(stat_input_intersection, unit_id):
        distinct_patches = set()
        patches_area = 0
        with SearchCursor(stat_input_intersection,
                          field_names=['SHAPE@AREA', 'patchID', UNIT_ID_FIELD_NAME],
                          where_clause='%s=%d' % (UNIT_ID_FIELD_NAME, unit_id)) as sc1:
            for r1 in sc1:
                patch_area = r1[0]
                patches_area += patch_area
                distinct_patches.add(r1[1])
        number_of_patches = len(distinct_patches)
        return number_of_patches, patches_area

    @staticmethod
    def _get_connections_area(stat_connections_intersection, unit_id):
        with SearchCursor(stat_connections_intersection, field_names=['SHAPE@AREA', 'unitID'],
                          where_clause='%s=%d' % (UNIT_ID_FIELD_NAME, unit_id)) as sc2:
            return sum([r2[0] for r2 in sc2])

    def _analyse_layer(self, params, layer_to_analyse, connections):
        stat_input_intersection = self.create_temp_layer('stat_input_intersection')
        stat_connections_intersection = self.create_temp_layer('stat_connections_intersection')
        arcpy.Intersect_analysis(in_features=[params.stat_layer, layer_to_analyse],
                                 out_feature_class=stat_input_intersection)
        arcpy.Intersect_analysis(in_features=[params.stat_layer, connections],
                                 out_feature_class=stat_connections_intersection)

        with UpdateCursor(params.stat_layer, [UNIT_ID_FIELD_NAME,  # 0 -hex ID
                                              ZONE_AREA_FIELD_NAME,  # 1 - hex area
                                              self.NUMBER_OF_PATCHES_FIELD_NAME,  # 2
                                              self.CONNECTION_AREA_FIELD_NAME,  # 3
                                              self.CONNECTION_AREA_PERCENTAGE_FIELD_NAME,  # 4
                                              self.PATCH_AREA_FIELD_NAME,  # 5
                                              self.PATCH_AREA_PERCENT_FIELD_NAME  # 6
                                              ]) as stat_layer_cursor:
            for stat_layer_row in stat_layer_cursor:
                unit_id = stat_layer_row[0]
                zone_area = stat_layer_row[1]

                number_of_patches, patches_area = self._get_patches_number_and_area(stat_input_intersection, unit_id)
                connections_area = self._get_connections_area(stat_connections_intersection, unit_id)

                stat_layer_row[2] = number_of_patches
                stat_layer_row[3] = connections_area
                stat_layer_row[4] = round(connections_area / zone_area * 100, 3)
                stat_layer_row[5] = patches_area
                stat_layer_row[6] = round(patches_area / zone_area * 100, 3)

                stat_layer_cursor.updateRow(stat_layer_row)

    def _simplify_input(self, input_layer_dissolved, conn_distance, max_allowable_distance):
        input_simplified = self.create_temp_layer('input_simplified')
        arcpy.SimplifyPolygon_cartography(in_features=input_layer_dissolved,
                                          out_feature_class=input_simplified,
                                          algorithm='POINT_REMOVE',
                                          tolerance=max_allowable_distance,
                                          collapsed_point_option=False)
        arcpy.Densify_edit(in_features=input_simplified, densification_method='DISTANCE', distance=conn_distance / 5)
        return input_simplified


class CreateHexagons(object):
    def __init__(self):
        # see http://resources.arcgis.com/en/help/main/10.1/index.html#//001500000024000000
        self.label = "Create hexagons"
        self.description = '''Creates a new hexagon layer based on a user defined hexagon height.<br/> Hexagons may be also centered to the specific point (defined by centroid of the given layer) to represent the relations between the distance from the center and the observed values.'''
        self.canRunInBackground = True
        # self.category = 'Creating'

    def getParameterInfo(self):
        # Define parameter definitions

        inputArea = arcpy.Parameter(
            displayName="Input layer",
            name="in_area",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Input")

        useExtent = arcpy.Parameter(
            displayName="Hexagon layer extent = display extent",
            name="use_extent",
            datatype="GPBoolean",
            parameterType="Required",
            direction="Input")
        useExtent.value = False

        clipToInput = arcpy.Parameter(
            displayName="Clip hexagon layer to input area",
            name="clip_to_input",
            datatype="GPBoolean",
            parameterType="Required",
            direction="Input")
        clipToInput.value = True

        hexHeight = arcpy.Parameter(
            displayName="Hexagon height",
            name="hex_height",
            datatype="GPDouble",
            parameterType="Required",
            direction="Input")
        hexHeight.value = 1000

        outFeatureClass = arcpy.Parameter(
            displayName="Output hexagon Layer",
            name="output_layer",
            datatype="DEShapefile",
            parameterType="Required",
            direction="Output")

        centerHexagons = arcpy.Parameter(
            displayName="Center hexagons",
            name="center_hexagons",
            datatype="GPBoolean",
            parameterType="Optional",
            direction="Input")

        centerFeatureLayer = arcpy.Parameter(
            displayName="Feature layer to center",
            name="center_fc",
            datatype="GPFeatureLayer",
            parameterType="Optional",
            direction="Input")

        params = [inputArea, useExtent, clipToInput, hexHeight, centerHexagons, centerFeatureLayer, outFeatureClass]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        input_parameters = ScriptParameters(parameters)
        if input_parameters.in_area.altered:
            # input_parameters.center_fc.value = input_parameters.in_area.value
            pass
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        setup_debug_mode()
        input_parameters = ScriptParameters(parameters)
        width = input_parameters.hex_height.value
        hexOut = input_parameters.output_layer.valueAsText
        inputArea = input_parameters.in_area.valueAsText
        boolExtent = input_parameters.use_extent.value
        clipToInput = input_parameters.clip_to_input.value
        centerHexagons = input_parameters.center_hexagons
        centerLayer = input_parameters.center_fc.valueAsText
        if centerHexagons and not centerLayer:
            centerLayer = inputArea

        scratchworkspace = get_scratchworkspace()
        log("create hexagon layer....")
        Fishnet_1 = scratchworkspace + "\\Fishnet1"
        Fishnet_2 = scratchworkspace + "\\Fishnet2"
        Fishnet_Label_1 = scratchworkspace + "\\Fishnet1_Label"
        Fishnet_Label_2 = scratchworkspace + "\\Fishnet2_Label"
        Appended_Points_Name = "hex_points"
        Appended_Points = scratchworkspace + "\\" + Appended_Points_Name

        delete_if_exists(Fishnet_1, Fishnet_2, Appended_Points)

        # Process: Calculate Value (width)...
        height = float(width) * math.sqrt(3)

        # Invert the height and width so that the flat side of the hexagon is on the bottom and top
        tempWidth = width
        width = height
        height = tempWidth

        log("height: " + str(height))
        log("width: " + str(width))

        # Process: Create Extent Information...
        ll = self.calculateOrigin(inputArea, boolExtent)
        Origin = self.updateOrigin(ll, width, height, -2.0)
        ur = self.calculateUR(inputArea, boolExtent)

        Opposite_Corner = self.updateOrigin(ur, width, height, 2.0)
        log("origin: " + Origin)
        log("opposite corner: " + Opposite_Corner)

        # Process: Calculate Value (Origin)...
        newOrigin = self.updateOrigin(Origin, width, height, 0.5)
        log("new origin: " + newOrigin)

        # Process: Calculate Value (Opposite Corner)...
        newOpposite_Corner = self.updateOrigin(Opposite_Corner, width, height, 0.5)
        log("newOpposite_Corner: " + newOpposite_Corner)

        # Process: Calculate Value (Y Axis 1)...
        Y_Axis_Coordinates1 = self.getYAxisCoords(Origin, Opposite_Corner)
        log("Y_Axis_Coordinates1: " + Y_Axis_Coordinates1)

        # Process: Create Fishnet...
        arcpy.CreateFishnet_management(Fishnet_1, Origin, Y_Axis_Coordinates1, width, height, "0", "0", Opposite_Corner,
                                       "LABELS", "")
        log("created fishnet 1...")
        arcpy.Delete_management(Fishnet_1)

        # Process: Calculate Value (Y Axis 2)...
        YAxis_Coordinates2 = self.getYAxisCoords(newOrigin, newOpposite_Corner)
        log("YAxis_Coordinates2: " + YAxis_Coordinates2)

        # Process: Calculate Value (Number of Columns)...
        Number_of_Columns = self.getCols(Origin, width, Opposite_Corner)
        log("Number_of_Columns: " + str(Number_of_Columns))

        # Process: Create Fishnet (2)...
        arcpy.CreateFishnet_management(Fishnet_2, newOrigin, YAxis_Coordinates2, width, height, "0", "0",
                                       newOpposite_Corner, "LABELS", "")
        log("created fishnet 2...")
        arcpy.Delete_management(Fishnet_2)

        # Process: Create Feature Class...
        arcpy.CreateFeatureclass_management(scratchworkspace, Appended_Points_Name, "POINT", "#", "#", "#",
                                            arcpy.Describe(inputArea).SpatialReference)
        log("created template fc...")

        # Process: Append...
        arcpy.Append_management(Fishnet_Label_1 + ";" + Fishnet_Label_2, Appended_Points, "NO_TEST", "", "")
        log("appended fishnets...")
        arcpy.Delete_management(Fishnet_Label_1)
        arcpy.Delete_management(Fishnet_Label_2)

        with createTempLayer('hexBeforeClip') as hexBeforeClip:

            # Process: Create Thiessen Polygons...
            # Limit Hexagons roughly to the real input data extent:
            arcpy.MakeFeatureLayer_management(Appended_Points, "in_memory\\hexPoints")
            arcpy.SelectLayerByLocation_management("in_memory\\hexPoints", "WITHIN_A_DISTANCE", inputArea, width * 2,
                                                   "NEW_SELECTION")
            arcpy.CreateThiessenPolygons_analysis("in_memory\\hexPoints", hexBeforeClip, "ONLY_FID")
            log("created thiessen polygons...")
            arcpy.Delete_management(Appended_Points)

            if centerHexagons:
                self.centerHexagons(centerLayer, hexBeforeClip)

            # Process: keep Hexagons only
            # if boolExtent == "false":
            if not clipToInput:
                log("keep only hexagons...")
                self.deleteNotHexagons(hexBeforeClip, inputArea, height, boolExtent)
                arcpy.CopyFeatures_management(hexBeforeClip, hexOut)
            else:
                log("clipping to input layer area...")
                arcpy.Clip_analysis(hexBeforeClip, inputArea, out_feature_class=hexOut)

            arcpy.AddField_management(hexOut, UNIT_ID_FIELD_NAME, "LONG", "", "", "", "", "NULLABLE", "NON_REQUIRED",
                                      "")
            log("added field unitID...")

            # Process: Calculate Hexagonal Polygon ID's...
            numberHexagons = self.calculateHexPolyID(hexOut)
            log("number of Units = " + str(numberHexagons))

            # Process: Add Spatial Index...
            gdb = os.path.dirname(hexOut)
            if gdb.find("mdb") != -1:
                log("personal")
            else:
                arcpy.AddSpatialIndex_management(hexOut)
                log("added Spatial Index...")

            log("Units prepared.....")

    def calculateHexPolyID(self, hexOut):
        fields = (UNIT_ID_FIELD_NAME,)
        with UpdateCursor(hexOut, fields) as cur:
            ID = 1
            for row in cur:
                row[0] = ID
                cur.updateRow(row)
                ID += 1
        return ID - 1

    def calculateOrigin(self, dataset, boolExtent):
        if boolExtent == "true":
            mxd = arcpy.mapping.MapDocument("CURRENT")
            df = mxd.activeDataFrame
            ext = df.extent
            return str(ext.XMin) + " " + str(ext.YMin)
        else:
            ext = arcpy.Describe(dataset).extent
            # coords = ext.split(" ")
            return str(ext.XMin) + " " + str(ext.YMin)

    def calculateUR(self, dataset, boolExtent):
        if boolExtent == "true":
            mxd = arcpy.mapping.MapDocument("CURRENT")
            df = mxd.activeDataFrame
            ext = df.extent
            return str(ext.XMax) + " " + str(ext.YMax)
        else:
            ext = arcpy.Describe(dataset).extent
            # coords = ext.split(" ")
            return str(ext.XMax) + " " + str(ext.YMax)

    def calculateHeight(self, width):
        return float(width) * math.sqrt(3)

    def updateOrigin(self, origin, width, height, factor):
        coords = origin.split(" ")
        xcoord = float(coords[0])
        ycoord = float(coords[1])
        return str(xcoord + float(width) * factor) + " " + str(ycoord + float(height) * factor)

    def getYAxisCoords(self, origin, opposite):
        origin_coords = origin.split(" ")
        xcoord_origin = float(origin_coords[0])
        corner_coords = opposite.split(" ")
        ycoord_opposite = float(corner_coords[1])
        return str(xcoord_origin) + " " + str(ycoord_opposite)

    def getCols(self, origin, width, opposite):
        coords = origin.split(" ")
        x_origin = float(coords[0])
        coords = opposite.split(" ")
        x_opposite = float(coords[0])
        return int((x_opposite - x_origin) / int(width))

    def deleteNotHexagons(self, input1, input2, height, boolExtent):
        geo = arcpy.Geometry()
        geometryList = arcpy.CopyFeatures_management(input2, geo)
        fuzzyArea = 2 * ((float(height) / 2) ** 2) * (3 ** 0.5)
        log("fuzzyArea: " + str(fuzzyArea))
        # unionpoly = arcpy.Polygon()
        i = 0
        for poly in geometryList:
            if i == 0:
                unionpoly = poly
            else:
                unionpoly = unionpoly.union(poly)
            i = 1

        x = 0
        with UpdateCursor(input1, ["SHAPE@"]) as cur:
            for row in cur:
                feat = row[0]
                if boolExtent == "false" and feat.disjoint(unionpoly):
                    cur.deleteRow()
                    x += 1
                elif feat.area > (fuzzyArea + 10) or feat.area < (fuzzyArea - 10):
                    cur.deleteRow()
                    x += 1
        log("deleted hexagons: " + str(x))

    def centerHexagons(self, centerLayer, hexesLayer):
        # move hexagons to center of input layer
        geometryList = arcpy.MinimumBoundingGeometry_management(in_features=centerLayer,
                                                                out_feature_class=arcpy.Geometry(),
                                                                geometry_type='CIRCLE', group_option='ALL',
                                                                mbg_fields_option='MBG_FIELDS')
        centroidOfInput = geometryList[0].centroid
        centroidOfHexes = centroidOfInput

        arcpy.AddField_management(hexesLayer, 'center_dst', "LONG")

        # search for center hexagon
        # TODO - should be a better way of finding center hexagon
        centerHexId = None
        with UpdateCursor(hexesLayer, ['SHAPE@', 'OID@', 'center_dst']) as sc:
            for row in sc:
                g = row[0]
                if g.contains(centroidOfInput):
                    centroidOfHexes = g.centroid
                    centerHexId = row[1]
                    row[2] = 0
                else:
                    row[2] = -1
                sc.updateRow(row)

        dx = centroidOfHexes.X - centroidOfInput.X
        dy = centroidOfHexes.Y - centroidOfInput.Y
        with UpdateCursor(hexesLayer, ['SHAPE@']) as sc:
            for row in sc:
                array = arcpy.Array()
                # Step through each part of the feature
                for part in row[0]:
                    # Step through each vertex in the feature
                    for pnt in part:
                        if pnt:
                            x = pnt.X - dx
                            y = pnt.Y - dy
                            np = arcpy.Point(x, y)
                            np.ID = pnt.ID
                            array.add(np)

                polygon = arcpy.Polygon(array)
                row[0] = polygon
                sc.updateRow(row)
        log('centerHexId: ' + str(centerHexId))

        with createTempLayer('hexSelection') as tmpSel:
            arcpy.MakeFeatureLayer_management(hexesLayer, tmpSel)

            with createTempLayer('previousHex') as previousHex:
                dst = 0
                arcpy.SelectLayerByAttribute_management(tmpSel, selection_type='NEW_SELECTION',
                                                        where_clause='OID=%d' % centerHexId)
                arcpy.CopyFeatures_management(tmpSel, previousHex)

                oids = [centerHexId]
                while oids:
                    dst += 1
                    log('Searching for hexagons within distance of %d units from center' % dst)
                    arcpy.SelectLayerByLocation_management(tmpSel, overlap_type='BOUNDARY_TOUCHES',
                                                           select_features=previousHex, selection_type='NEW_SELECTION')
                    arcpy.SelectLayerByAttribute_management(tmpSel, selection_type='SUBSET_SELECTION',
                                                            where_clause='center_dst=-1')
                    delete_if_exists(previousHex)
                    arcpy.CopyFeatures_management(tmpSel, previousHex)
                    oids = [str(row[0]) for row in SearchCursor(tmpSel, ['OID@'])]
                    # log('found oids: ' + str(oids))
                    if not oids:
                        # no more hexagons
                        break
                    with UpdateCursor(hexesLayer, field_names=['OID@', 'center_dst', '*'],
                                      where_clause='OID IN (%s)' % (','.join(oids))) as uc:
                        for row in uc:
                            row[1] = dst
                            uc.updateRow(row)

                            # arcpy.Delete_management(hexesLayer)
                            # arcpy.CopyFeatures_management(tmpSel, hexesLayer)


class CreatePie(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        # see http://resources.arcgis.com/en/help/main/10.1/index.html#//001500000024000000
        self.label = "Create pie layer"
        self.description = """Creates layer dividing the input area (defined by the to be analysed layer) into a defined number of sections (similar to pie charts, but with equal arc length).<br/>
        The sections are geographically oriented, that means that the first sector is always directed to the North, and e. g. when 4 sections would set in the properties, the whole area will be divided to the 90 degree sections directed to the four sides of the world."""
        self.canRunInBackground = True
        # self.category = 'Creating'

    def getParameterInfo(self):
        # Define parameter definitions

        # Input layer - a layer defining the extent of the pie layer (usually the layer for which the metrics will be calculated)
        inputArea = arcpy.Parameter(
            displayName="Input layer",
            name="in_area",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Input")

        # Number of sections - the number of pie sections to be calculated. Pie section will be geograpically oriented, the first is directed to the North. Usually  numbers divisible by 4 are recommended
        sections_number = arcpy.Parameter(
            displayName="Number of sections",
            name="sections_number",
            datatype="GPLong",
            parameterType="Required",
            direction="Input")
        sections_number.value = 8

        # Output pie layer - the name for the output pie layer
        outFeatureClass = arcpy.Parameter(
            displayName="Output pie layer",
            name="output_layer",
            datatype="DEShapefile",
            parameterType="Required",
            direction="Output")

        params = [inputArea, sections_number, outFeatureClass]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        parameters[1].clearMessage()
        if int(parameters[1].value) < 2:
            parameters[1].setErrorMessage('Number of sections must be value greater than 1')
        return

    def execute(self, parameters, messages):
        setup_debug_mode()
        import cmath

        input_parameters = ScriptParameters(parameters)
        inputArea = input_parameters.in_area.valueAsText
        n = int(input_parameters.sections_number.valueAsText)
        out = input_parameters.output_layer.valueAsText

        bound = 'in_memory\\bound_circle%d' % random.randint(0, 1000000)
        points = 'bound_point%d' % random.randint(0, 1000000)
        thies = 'in_memory\\thiessen%d' % random.randint(0, 1000000)
        try:
            arcpy.CreateFeatureclass_management(get_scratchworkspace(), points, "POINT", "#", "#", "#",
                                                arcpy.Describe(inputArea).SpatialReference)
            points = 'in_memory\\' + points
            arcpy.MinimumBoundingGeometry_management(inputArea, bound, "CIRCLE", "ALL", mbg_fields_option="MBG_FIELDS")
            arcpy.AddField_management(points, UNIT_ID_FIELD_NAME, "LONG", "", "", "", "", "NULLABLE", "NON_REQUIRED",
                                      "")
            d = 0
            centroid = None
            with SearchCursor(bound, ('MBG_Diameter', 'SHAPE@TRUECENTROID')) as c:
                row = next(c)
                d = row[0]
                centroid = row[1]

            phiInRadians = 2 * math.pi / n
            r = d / 2.
            initialAngle = math.pi / 2

            with InsertCursor(points, ('SHAPE@', UNIT_ID_FIELD_NAME)) as cur:
                for i in range(0, n):
                    rect = cmath.rect(r, phiInRadians * i + initialAngle)
                    rect = (rect.real + centroid[0], rect.imag + centroid[1])
                    point = arcpy.Point()
                    point.X = rect[0]
                    point.Y = rect[1]
                    cur.insertRow((point, i + 1))

            arcpy.CreateThiessenPolygons_analysis(points, thies, "ALL")
            arcpy.Clip_analysis(thies, inputArea, out_feature_class=out)

        except Exception as e:
            handleException(e)
        finally:
            delete_if_exists(points, bound, thies)
