wcc_dict = {
    0: 0,
    1: 0.41853,
    2: 0.73279,
    5: 1.43842,
    10: 2.29088,
    15: 2.96326,
    20: 3.53682,
    30: 3.53682,
    45: 5.70447,
    50: 6.05853,
    60: 6.71803,
    75: 7.61312,
    90: 8.42355,
    100: 8.92714,
    150: 11.13557,
    200: 12.99981,
    300: 16.13012,
    400: 18.77086,
    500: 21.09824,
    600: 23.20286,
    700: 25.13853,
    800: 26.94043,
    900: 28.63298,
    1000: 30.2339
}


def get_wcc(value):
    """Returns the WCC value for input cell size. If value not in dict, returning default value for cells ize = 15"""
    return wcc_dict.get(value, 15)

